/**
 *  DobyGames Import Script
 *	  for kikakudane2 chaineder
 */

// Plugin files
var docroot_plugins = 'js/plugins/';
var arr_plugins = new Array(
		"nineleap.enchant.js"
	);
for(i in arr_plugins){
	document.write('<script type="text/javascript" src="'+ docroot_plugins + arr_plugins[i] + '"></script>');
}

// Initialize enchant.js
enchant();

// Class files
var docroot_class = 'js/class/';
var arr_class = new Array(
		"myprototype.js",
		"sequence.js",
		"Model/sceneState.js",
		"Model/starting.js",
		"Model/prepare.js",
		"Model/battle.js",
		"Model/lobby.js",
		"Model/debug.js",
		"View/startingScene.js",
		"View/prepareScene.js",
		"View/battleScene.js",
		"View/lobbyScene.js",
		"View/debugScene.js",
		"View/Sprite/spriteBase.js",
		"View/Sprite/spriteUnit.js",
		"View/Sprite/spriteBattler.js",
		"View/Sprite/spriteGauge.js",
		"View/Sprite/spriteEffect.js",
		"View/Sprite/spriteNumber.js",
		"View/Sprite/spriteDamage.js",
		"View/Sprite/spriteCrystal.js",
		"View/Surface/advancedSurface.js",
		"Controller/Controller.js",
		"Controller/startingController.js",
		"Controller/prepareController.js",
		"Controller/battleController.js",
		"Controller/lobbyController.js",
		"Controller/debugController.js",
		"unitManager.js",
		"formation.js",
		"arrangement.js",
		"chainCounter.js",
		"stock.js",
		"job.js",
		"chain.js",
		"synchroLine.js",
		"synchroChecker.js",
		"unit.js",
		"battler.js",
		"challenger.js",
		"test.js"
	);

// Setting files
var docroot_settings = 'js/settings/';
var arr_settings = new Array(
		"chaineder_ui.js"
	);

// Import
for(i in arr_class){
	document.write('<script type="text/javascript" src="'+ docroot_class + arr_class[i] + '"></script>');
}
for(i in arr_settings){
	document.write('<script type="text/javascript" src="'+ docroot_settings + arr_settings[i] + '"></script>');
}
