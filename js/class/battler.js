/**
 * battler.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Battler = new function() {
	var maxhp, hp, str, def, mgc;
	var formation;
	var type;
	var battler_name;
	var log;

	var self = function Battler(formation) {
		if (typeof formation === 'undefined') {
			formation = new Formation();
			formation.addUnit("Knight");
		}
		this.formation = formation;
		// Initialize Status
		this.maxhp	= this.formation.getMaxHp();
		this.hp		= this.formation.getMaxHp();
		this.str	= this.formation.getStr();
		this.def	= this.formation.getDef();
		this.mgc	= this.formation.getMgc();
		this.type	= this.formation.getType();
		this.log	= new Array();
		this.batler_name	= ""; 
		this.ai	= 0;			// 戦闘AI 0:no change, 1:random, 2:test case
		this.patterns	= new Array();	
		this.crt_base	= 5;	// 基礎クリティカル率
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getMaxHp: function() {
			return this.maxhp;
		}
		, getHp: function() {
			return this.hp;
		}
		, getStr: function() {
			return this.str;
		}
		, getDef: function() {
			return this.def;
		}
		, getMgc: function() {
			return this.mgc;
		}
		, getType: function() {
			return this.type;
		}
		, getJobType: function() {
			var type = this.getType();
			var job_type = "";
			switch (type) {
				case "str":
					job_type = "K";
					break;
				case "def":
					job_type = "G";
					break;
				case "mgc":
					job_type = "W";
					break;
			}
			return job_type;
		}
		, getTypeAlias: function() {
			var job_type = this.getJobType();
			var job = new Job(job_type);
			return job.getAlias();
		}
		, getBattlerName: function() {
			return this.battler_name;
		}
		, getCrt: function() {
			var crt = this.crt_base;
			if (this.getType() == "mgc") {
				crt += 5;	// クリティカル率のボーナス
			}
			return crt;
		}
		, getAtkVal: function() {
			var type = this.getType();
			var atk = 0;
			switch (type) {
				case 'str':
					atk = this.getStr();
					break;
				case 'def':
					atk = this.getDef();
					break;
				case 'mgc':
					atk = this.getMgc();
					break;
				default:
					break;
			}
			atk = Math.floor(atk);
			return atk;
		}
		, getDefVal: function(attacker) {
			if (typeof attacker === 'undefined') return false;
			// 相手の攻撃属性から自分の防御属性を決める
			var def_type = this.getDefType(attacker);
			// def_typeは自分の防御属性
			var def = 0;
			switch (def_type) {
				case 'str':
					def = this.getStr();
					break;
				case 'def':
					def = this.getDef();
					break;
				case 'mgc':
					def = this.getMgc();
					break;
				default:
					break;
			}
			def = Math.floor(def);
			return def;
		}
		, getDefType: function(attacker) {
			if (typeof attacker === 'undefined') return false;
			// 相手のタイプを取得
			var atk_type = attacker.getType();
			var def_type = "";
			// 火は水、水は雷、雷は火で受けるじゃんけん方式
			switch (atk_type) {
				case 'str':
					def_type = "def";
					break;
				case 'def':
					def_type = "mgc";
					break;
				case 'mgc':
					def_type = "str";
					break;
				default:
					break;
			}
			return def_type;
		}
		, getAI: function() {
			return this.ai;
		}
		, getPatterns: function() {
			return this.patterns;
		}
		, getUnits: function() {
			return this.formation.getUnits();
		}

		/**
		 * boolean
		 */
		, isDead: function() {
			if (this.hp <= 0) {
				return true;
			}
			return false;
		}
		, isLive: function() {
			if (0 < this.hp) {
				return true;
			}
			return false;
		}

		/**
		 * setter
		 */
		, setBattlerName: function(name) {
			this.battler_name = name;
			return this.battler_name;
		}
		, setFormation: function(formation) {
			this.formation = formation;
			this.update();
			return this.formation;
		}
		, setList: function(list) {
			this.formation.setList(list);
			this.update();
			return this.formation.getList();
		}
		, setAI: function(ai) {
			this.ai = ai;
		}
		, setPatterns: function(patterns) {
			this.patterns = patterns;
		}

		/**
		 * battler
		 */
		, completeRecovery: function() {
			this.hp = this.getMaxHp();
			return this.hp;
		}
		, damage: function(value) {
			this.hp -= value;
			if (this.hp <= 0) {
				this.hp = 0;
			}
		}
		, shuffleFormation: function() {
			var ai = this.getAI();
			// AIによって変更の方法を変える
			if (ai == 1) {
				// ランダムに陣形を変更する
				var count = 1;
				// count回だけユニットの位置を変える
				for(var i=0; i<count; i++) {
					// shuffle formation
					this.formation.shuffle();
				}
			} else if (ai == 2) {
				// パターンから陣形を変更する
				var patterns = this.getPatterns();
				if (patterns) {
					var rand = Math.floor(Math.random() * patterns.length);
					var pattern = patterns[rand];
					// change formation
					this.setList(pattern);
				}
			}
			// Update Arrangement
			this.update();
		}

		/**
		 * update
		 */
		, update: function() {
			this.formation.update();
			this.maxhp	= this.formation.getMaxHp();
			this.str	= this.formation.getStr();
			this.def	= this.formation.getDef();
			this.mgc	= this.formation.getMgc();
			this.type	= this.formation.getType();
		}

		/**
		 * debug method
		 */
		, viewList: function() {
			var str = this.formation.arrangement.viewList();
			return str;
		}
		, viewStatus: function() {
			var str ="";
			str += "HP :" + this.getHp() + ", ";
			str += "str:" + this.getStr() + ", ";
			str += "def:" + this.getDef() + ", ";
			str += "mgc:" + this.getMgc() + ", ";
			str += "type:" + this.getType() + ", ";
			return str;
		}
	};

	return self;
}


