/**
 * chain.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Chain = new function() {
	var sameJobChain	= 0;
	var brChain		= 0;
	var trChain		= 0;
	var lonely		= true;

	var self = function Chain(params) {
		// 連結数はタテとヨコで別々に持つ
		var chain_matrix = new Object();
		chain_matrix.row  = 0;
		chain_matrix.line = 0;

		// 未指定の場合に入る初期値
		var defaults = {
			'sameJobChain' : chain_matrix,
			'brChain' : chain_matrix,
			'trChain' : chain_matrix,
			'lonely'  : true
		}
		if (typeof params === 'undefined') params = defaults;

		// sameJobChain
		if (typeof params.sameJobChain !== 'undefined') {
			this.sameJobChain = params.sameJobChain;
		} else {
			this.sameJobChain = defaults.sameJobChain;
		}
		// brChain
		if (typeof params.brChain !== 'undefined') {
			this.brChain = params.brChain;
		} else {
			this.brChain = defaults.brChain;
		}
		// trChain
		if (typeof params.trChain !== 'undefined') {
			this.trChain = params.trChain;
		} else {
			this.trChain = defaults.trChain;
		}
		// lonely
		if (typeof params.lonely !== 'undefined') {
			this.lonely = params.lonely;
		} else {
			this.lonely = defaults.lonely;
		}
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getSameJobChain: function() {
			return this.sameJobChain;
		}
		, getBrChain: function() {
			return this.brChain;
		}
		, getTrChain: function() {
			return this.trChain;
		}
		, getLonely: function() {
			return this.lonely;
		}
	};

	return self;
}

