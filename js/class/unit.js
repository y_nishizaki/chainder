/**
 * unit.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Unit = new function() {
	var sprite;	//@ has a Sprite Class.
	var job;	//@ Knight, Guardian, Wizard, Bridge, Terminator, Berserker
	var chain;
	var maxhp, str, def, mgc;
	var highest, lowest;
	var rowIndex, lineIndex;

	var self = function Unit(job_name, chain_params) {
		// Require Job, Chain
		if (typeof job_name === 'undefined') job_name = "Nobody";
		this.job = new Job(job_name);
		if (typeof chain_params === 'undefined') chain_params = new Array();
		this.chain = new Chain(chain_params);
		this.update();
	}

	self.prototype = {
		constructor: self
		/**
		 * getter method
		 */
		, getMaxHp: function() {
			this.maxhp	= this.job.getMaxHp();
			return this.maxhp;
		}
		, getStr: function() {
			var str_base	= this.getParamBase("str");
			var row_bonus	= this.getRowBonus("str");
			var line_bonus	= this.getLineBonus("str");
			this.str = str_base + row_bonus + line_bonus;
			return this.str;
		}
		, getDef: function() {
			var def_base	= this.getParamBase("def");
			var row_bonus	= this.getRowBonus("def");
			var line_bonus	= this.getLineBonus("def");
			this.def = def_base + row_bonus + line_bonus;
			return this.def;
		}
		, getMgc: function() {
			var mgc_base	= this.getParamBase("mgc");
			var row_bonus	= this.getRowBonus("mgc");
			var line_bonus	= this.getLineBonus("mgc");
			this.mgc = mgc_base + row_bonus + line_bonus;
			return this.mgc;
		}
		, getJobType: function() {
			return this.job.getType();
		}
		, getJobAbbr: function() {
			return this.job.getAbbr();
		}

		/**
		 * Chain Bonus in getParam()
		 */
		, getRowBonus: function(param_type) {
			// base
			var param_base = this.getParamBase(param_type);
			// chain
			var sameJobChain = this.chain.getSameJobChain();
			var brChain		 = this.chain.getBrChain();
			var trChain		 = this.chain.getTrChain();
			// multiply power default
			var sameJobPower = 1;
			var brPower		 = 1;
			var trPower		 = 1;
			// row bonus
			var row_bonus = 0;
		  	if (this.job.highest == param_type) {
				if (sameJobChain.row) { 
					sameJobPower = 1.5;
				}
				if (brChain.row) {
					brPower = brChain.row * 2;
				}
				row_bonus = (param_base * sameJobPower * brPower) - param_base;
			}
		  	if (this.job.lowest == param_type) {
				if (trChain.row) {
					trPower = trChain.row * 5;
				}
				row_bonus = (param_base * trPower) - param_base;
			}
			return row_bonus;
		}
		, getLineBonus: function(param_type) {
			// base
			var param_base = this.getParamBase(param_type);
			// chain
			var sameJobChain = this.chain.getSameJobChain();
			var brChain		 = this.chain.getBrChain();
			var trChain		 = this.chain.getTrChain();
			// multiply power default
			var sameJobPower = 1;
			var brPower		 = 1;
			var trPower		 = 1;
			// line bonus
			var line_bonus = 0;
		  	if (this.job.highest == param_type) {
				if (sameJobChain.line) { 
					sameJobPower = 1.5;
				}
				if (brChain.line) {
					brPower = brChain.line * 2;
				}
				line_bonus = (param_base * sameJobPower * brPower) - param_base;
			}
		  	if (this.job.lowest == param_type) {
				if (trChain.line) {
					trPower = trChain.line * 5;
				}
				line_bonus = (param_base * trPower) - param_base;
			}
			return line_bonus;
		}
		, getParamBase: function(param_type) {
			var param_base = "";
			if (param_type == "str") {
				param_base = this.job.getStr(); 
			} else if (param_type == "def") {
				param_base = this.job.getDef(); 
			} else if (param_type == "mgc") {
				param_base = this.job.getMgc(); 
			}
			// ArroundWeaker Effect
		  	if (this.job.isArroundWeaker() && !this.isLonely()) {
		  		param_base = 1;
		  	}
			return param_base;
		}

		/**
		 * boolean
		 */
		, isLonely: function() {
			return this.chain.getLonely();
		}

		/**
		 *  setter method
		 */
		, setJob: function(job) {
			this.job = job;
		}
		, setChain: function(chain) {
			this.chain = chain;
		}

		/**
		 * Change Job method
		 */
		, changeJob: function(job_name) {
			if (typeof job_name === 'undefined') job_name = "Nobody";
			this.job = new Job(job_name);
			return this.job;
		}

		/**
		 *  update method
		 */
		, update: function() {
			this.highest	= this.job.getHighest();
			this.lowest		= this.job.getLowest();
			this.getMaxHp();
			this.getStr();
			this.getDef();
			this.getMgc();
		}
	};

	return self;
}

