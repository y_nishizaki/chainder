/**
 * challenger.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Challenger = new function() {
	var id;
	var name;
	var type;				// 0:random, 1:fix
	var default_list;		// in case of fix
	var list;
	var early_list;			// 初期陣形
	var job_type;			// Knight, Guardian, Wizard, Berserker. in makeRandomList()
	var level;				// 0:easy, 1:normal, 2:hard, 3:very hard. in makeRandomList()
	var knock_down;			// bool
	var explain;			// text
	var battle_point;		// str + def + mgc
	var ai;					// 0:no change, 1:random, 2:test case
	var patterns;			// pattern test case

	var self = function Challenger(id) {
		if (typeof id === 'undefined') id = 0;
		// Common Settings
		this.list = new Array();
		this.knock_down = false;
		// Set Challenger by id
		this.setChallenger(id);
		// Set Early List
		this.setEarlyList();
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getId: function() {
			return this.id;
		}
		, getName: function() {
			return this.name;
		}
		, getList: function() {
			// 初期陣形を返す
			this.list = this.early_list;
			return this.list;
		}
		, getType: function() {
			return this.type;
		}
		, getJobType: function() {
			return this.job_type;
		}
		, getLevel: function() {
			return this.level;
		}
		, getExplain: function() {
			return this.explain;
		}
		, getBattlePoint: function() {
			var list = this.getList();
			if (list) {
				// arrangementを生成
				var arrangement = new Arrangement(list);	
				// str, def, mgcの合計値を返す
				var battle_point = 0;
				battle_point += arrangement.getStr();
				battle_point += arrangement.getDef();
				battle_point += arrangement.getMgc();
				return parseInt(battle_point);
			}
			return false;
		}
		, getAI: function() {
			this.ai = 0;
			// AIはTypeによって決まる
			var type = this.getType();
			if (type == 0) {
				this.ai = 1;
			} else if (type == 1) {
				this.ai = 2;
			}
			return this.ai;
		}
		, getPatterns: function() {
			if (typeof this.patterns === 'undefined') {
				return false;
			}
			return this.patterns;
		}

		/**
		 * boolean
		 */
		, isKnockDown: function() {
			return this.knock_down;
		}

		/**
		 * setter
		 */
		, setKnockDown: function(bool) {
			this.knock_down = bool;
		}
		, setEarlyList: function() {
			var type = this.getType();
			// ランダムとテストケース有りで分岐させて初期陣形を作成する
			switch (type) {
				case 0:
					//  this.early_list = [
					//  	["K",  "K",  "K",  "K",  "Br"],
					//  	["G",  "G",  "G",  "",  ""],
					//  	["W",  "W",  "W",  "",  ""]
					//  ];

					// 陣形のランダム生成に関係する変数
					var job_type = this.getJobType();
					var level = this.getLevel();
					// ランダムな初期陣形を生成する
					this.early_list = this.makeRandomList(job_type, level);
					break;
				case 1:
					// テストケースから初期陣形を生成する
					this.early_list = this.default_list;
					break;
				default:
					break;
			}
		}

		, makeRandomList: function(job_type, level) {
			if (typeof job_type === 'undefined') job_type = 'K';
			if (typeof level === 'undefined') level = 1;

			// ユニットの生成
			var own_units = new Array();
			switch (level) {
				case 1:
					/**
					 * Level 1
					 */
					switch (job_type) {
						case 'K':
							own_units = { 
								"K": 5, 
								"R" : 6 
							}
							break;
						case 'G':
							own_units = { 
								"G": 5, 
								"R" : 6 
							}
							break;
						case 'W':
							own_units = { 
								"W": 5, 
								"R" : 6 
							}
							break;
						case 'Tr':
							own_units = { 
								"K": 3, 
								"G": 3, 
								"W": 3, 
								"Tr" : 2 
							}
							break;
						case 'Bs':
							own_units = { 
								"Bs": 4, 
							}
							break;
					}
					break;
				case 2:
					/**
					 * Level 2
					 */
					switch (job_type) {
						case 'K':
							own_units = { 
								"K": 8, 
								"R" : 5 
							}
							break;
						case 'G':
							own_units = { 
								"G": 7, 
								"R" : 6 
							}
							break;
						case 'W':
							own_units = { 
								"W": 9, 
								"R" : 4 
							}
							break;
						case 'Tr':
							own_units = { 
								"K": 3, 
								"G": 3, 
								"W": 3, 
								"Tr" : 2,
								"Br" : 2 
							}
							break;
						case 'Bs':
							own_units = { 
								"Bs": 5, 
							}
							break;
					}
					break;
				case 3:
					/**
					 * Level 3
					 */
					switch (job_type) {
						case 'K':
							own_units = { 
								"K": 9, 
								"R" : 6 
							}
							break;
						case 'G':
							own_units = { 
								"G": 9, 
								"R" : 6 
							}
							break;
						case 'W':
							own_units = { 
								"W": 9, 
								"R" : 6 
							}
							break;
						case 'Tr':
							own_units = { 
								"K": 3, 
								"G": 3, 
								"W": 3, 
								"Tr" : 2,
								"Br" : 2,
								"R" : 2 
							}
							break;
						case 'Bs':
							own_units = { 
								"Bs": 6, 
							}
							break;
					}
					break;
				default:
					break;
			}

			// foramtion
			var formation = new Formation();
			for (var key in own_units) {
				var value = own_units[key];
				for (var i=0; i<value; i++) {
					formation.addUnit(key);
				}
			}
			// shuffle position
			var rand = Math.floor(Math.random()*20);
			for (var i=0; i<rand; i++) {
				formation.shuffle();
			}
			formation.update();
			// list
			var list = formation.getList();

			return list;
		}

		, setChallenger: function(id) {
			if (typeof id === 'undefined') id = 0;

			switch (id) {
				/**
				 * Test 
				 */
				case 0:
					this.id = 0;
					this.name = "チャレンジャー";
					this.type = 1;
					this.default_list = [
						["K",  "K",  "K",  "",  ""],
						["",  "G",  "",  "",  ""],
						["",  "G",  "",  "",  ""]
					];
					this.job_type = "K";
					this.level = 0;
					break;

				/**
				 * Level 1
				 */
				case 1:
					this.id = 1;
					this.name = "光の神バルドル";
					this.type = 1;
					this.default_list = [
						["W",  "",  "K",  "",  "W"],
						["K",  "K",  "Br",  "K",  "K"],
						["W",  "",  "K",  "",  "W"]
					];
					this.patterns = [
						[
							["W",  "",  "K",  "",  "W"],
							["K",  "K",  "Br",  "K",  "K"],
							["W",  "",  "K",  "",  "W"]
						],
						[
							["W",  "K",  "K",  "",  "W"],
							["K",  "",  "Br",  "",  "K"],
							["W",  "",  "K",  "K",  "W"]
						],
						[
							["",  "",  "K",  "W",  "W"],
							["K",  "K",  "Br",  "K",  "K"],
							["",  "",  "K",  "W",  "W"]
						],
						[
							["",  "",  "K",  "W",  "W"],
							["K",  "K",  "Br",  "W",  "W"],
							["",  "",  "K",  "K",  "K"]
						],
						[
							["",  "",  "K",  "",  ""],
							["K",  "K",  "K",  "K",  "K"],
							["W",  "W",  "Br",  "W",  "W"]
						]
					];
					this.job_type = "K";
					this.level = 1;
					this.explain = "精霊王として君臨することが夢だった。<br>精霊たちに喜んでもらえる国を作ろうと戦う。";
					break;
				case 2:
					this.id = 2;
					this.name = "詩神ブラギ";
					this.type = 1;
					this.default_list = [
						["G",  "Br",  "G",  "",  "Bs"],
						["",  "",  "Br",  "",  ""],
						["Bs",  "",  "G",  "Br",  "G"],
					];
					this.patterns = [
						[
							["G",  "Br",  "G",  "",  "Bs"],
							["",  "",  "Br",  "",  ""],
							["Bs",  "",  "G",  "Br",  "G"],
						],
						[
							["",  "Br",  "G",  "",  "Bs"],
							["G",  "",  "Br",  "",  "G"],
							["Bs",  "",  "G",  "Br",  ""],
						],
						[
							["G",  "Br",  "Br",  "G",  "Bs"],
							["",  "",  "",  "",  ""],
							["Bs",  "",  "G",  "Br",  "G"],
						]
					];
					this.job_type = "G";
					this.level = 1;
					this.explain = "精霊たちの言霊の乱れを懸念して、教育に改革をもたらす<br>「美しい言霊」政策の実行を決断。精霊王戦へ出馬する。<br>教え子に自分の代わりに戦う将となるよう自慢の詩で口説いた。";
					break;
				case 3:
					this.id = 3;
					this.name = "林檎の管理者イズン";
					this.type = 0;
					this.default_list = [
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""]
					];
					this.job_type = "W";
					this.level = 1;
					this.explain = "夫ブラギの悲願成就のため、<br>自らも「美しい言霊」政策を掲げ精霊王戦へ打って出る。<br>しかし裏ではしたたかに知恵の実量産計画も進める。";
					break;
				case 4:
					this.id = 4;
					this.name = "司法神フォルセティ";
					this.type = 0;
					this.default_list = [
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""]
					];
					this.job_type = "Tr";
					this.level = 1;
					this.explain = "アースガルズでは長すぎる寿命が精霊たちの問題だ。<br>アースガルド法第２０条を改正し、<br>平穏死を認めさせるよう精霊王を目指す。";
					break;
				case 5:
					this.id = 5;
					this.name = "復讐者ヴァーリ";
					this.type = 1;
					this.default_list = [
						["Bs",  "",  "Bs",  "",  "Bs"],
						["",  "",  "",  "",  ""],
						["Bs",  "",  "",  "",  "Bs"]
					];
					this.job_type = "Bs";
					this.level = 1;
					this.explain = "１００００年前の精霊王戦で負けた復讐を誓い参加。<br>今回の戦いに備え、報酬として魔石技術の密輸を行い<br>アルフヘイムから凄腕のエルフを雇い入れた。";
					break;

				/**
				 * Level 2
				 */
				case 6:
					this.id = 6;
					this.name = "太陽の女神ソール";
					this.type = 0;
					this.default_list = [
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""]
					];
					this.job_type = "K";
					this.level = 2;
					this.explain = "火の国ムスペルヘイムと協定を結び、<br>太陽の運行時間を長くしようとしている。<br>夜の来ない世界の実現を夢見て精霊王を志す。";
					break;
				case 7:
					this.id = 7;
					this.name = "月の神マー二";
					this.type = 0;
					this.default_list = [
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""],
						["",  "",  "",  "",  ""]
					];
					this.job_type = "G";
					this.level = 2;
					this.explain = "人口の多くなったミズガルズの人々を<br>月に移住させアースガルズの領土拡大を目指す。<br>太陽の光で死ぬドワーフたちからソール撃退を望まれている。";
					break;
				case 8:
					this.id = 8;
					this.name = "虹橋の番人ヘイムダル";
					this.type = 1;
					this.default_list = [
						["W",  "W",  "Br",  "Br",  "W"],
						["W",  "G",  "G",  "G",  "G"],
						["Tr",  "K",  "K",  "K",  "K"]
					];
					this.patterns = [
						[
							["W",  "W",  "Br",  "Br",  "W"],
							["W",  "G",  "G",  "G",  "G"],
							["Tr",  "K",  "K",  "K",  "K"]
						]
					];
					this.job_type = "W";
					this.level = 2;
					this.explain = "新たな魔石開発のため、精霊の交流を活発にする、<br>虹の橋「ビフレスト」の増設を計画している。<br>距離を無効化する耳と目で自らの将を選び出した。";
					break;
				case 9:
					this.id = 9;
					this.name = "豊穣神フレイ";
					this.type = 1;
					this.default_list = [
						["K",  "K",  "Br",  "K",  "K"],
						["G",  "G",  "Tr",  "G",  "G"],
						["W",  "W",  "Tr",  "W",  "W"]
					];
					this.patterns = [
						[
							["K",  "K",  "Br",  "K",  "K"],
							["G",  "G",  "Tr",  "G",  "G"],
							["W",  "W",  "Tr",  "W",  "W"]
						],
						[
							["K",  "K",  "Tr",  "K",  "K"],
							["G",  "G",  "Tr",  "G",  "G"],
							["W",  "W",  "Br",  "W",  "W"]
						],
						[
							["K",  "K",  "Tr",  "K",  "K"],
							["G",  "G",  "Br",  "G",  "G"],
							["W",  "W",  "Tr",  "W",  "W"]
						]
					];
					this.job_type = "Tr";
					this.level = 2;
					this.explain = "最上級の神名、豊穣の名を授かった責任感から<br>「豊かなアースガルズ」政策を唱える。<br>言霊の力を使ってヴァナヘイムとの大規模貿易交渉を望む。";
					break;
				case 10:
					this.id = 10;
					this.name = "終わらせる者ロキ";
					this.type = 1;
					this.default_list = [
						["Bs",  "",  "Bs",  "",  "Bs"],
						["",  "",  "",  "",  ""],
						["K",  "K",  "Br",  "K",  "Tr"]
					];
					this.patterns = [
						[
							["Bs",  "",  "Bs",  "",  "Bs"],
							["",  "",  "",  "",  ""],
							["K",  "K",  "Br",  "K",  "Tr"]
						],
						[
							["Bs",  "",  "Bs",  "",  "Bs"],
							["",  "",  "",  "",  ""],
							["K",  "K",  "K",  "Tr",  "Br"]
						]
					];
					this.job_type = "Bs";
					this.level = 2;
					this.explain = "理性の前にひれ伏せアースガルズよ。<br>圧倒的な知性を持ち、不条理をねじ伏せるものが、王。<br>われこそ精霊王として君臨するべきだ。";
					break;

				/**
				 * Level 3
				 */
				case 11:
					this.id = 11;
					this.name = "軍神テュール";
					this.type = 1;
					this.default_list = [
						["Tr",  "K",  "K",  "K",  "Tr" ],
						["K", "Br",  "Br",	"K",  "K"  ],
						["Tr",  "K",  "K",	"K",  "Tr"  ]
					];
					this.patterns = [
						[
							["Tr",  "K",  "K",  "K",  "Tr" ],
							["K", "Br",  "Br",	"K",  "K"  ],
							["Tr",  "K",  "K",	"K",  "Tr"  ]
						],
						[
							["Tr",  "K",  "K",  "K",  "Tr" ],
							["Br", "K",  "K",	"K",  "Br"  ],
							["Tr",  "K",  "K",	"K",  "Tr"  ]
						]
					];
					this.job_type = "K";
					this.level = 3;
					this.explain = "アースガルズから巨人族への敵対行動を<br>容認するために精霊王を目指す。<br>ニタヴェリールからの賄賂をもらったといううわさも。";
					break;
				case 12:
					this.id = 12;
					this.name = "海と風の神ニョルズ";
					this.type = 1;
					this.default_list = [
						["G",  "G",  "Br",  "G",  "G"],
						["G",  "G",  "Br",  "Br",  "G"],
						["Tr",  "W",  "W",  "W",  "Tr"],
					];
					this.patterns = [
						[
							["G",  "G",  "Br",  "G",  "G"],
							["G",  "G",  "Br",  "Br",  "G"],
							["Tr",  "W",  "W",  "W",  "Tr"],
						],
						[
							["Tr",  "G",  "G",  "G",  "Tr"],
							["G",  "G",  "G",  "Br",  "G"],
							["W",  "Br",  "Br",  "W",  "W"],
						]
					];
					this.job_type = "G";
					this.level = 3;
					this.explain = "ニブルヘイムとの領海領空権を巡る争いに<br>終止符を打つため、精霊王戦へ。<br>ミズガルズとの人材交流政策も精霊たちに支持されている。";
					break;
				case 13:
					this.id = 13;
					this.name = "雷神トール";
					this.type = 1;
					this.default_list = [
						["Tr",  "W",  "W",  "G",  "G"],
						["W",  "W",  "Br",  "Br",  "W"],
						["W",  "Br",  "W",  "G",  "W"]
					];
					this.patterns = [
						[
							["Tr",  "W",  "W",  "G",  "G"],
							["W",  "W",  "Br",  "Br",  "W"],
							["W",  "Br",  "W",  "G",  "W"]
						],
						[
							["G",  "Br",  "Br",  "G",  "G"],
							["W",  "W",  "W",  "W",  "W"],
							["W",  "Br",  "W",  "W",  "Tr"]
						]
					];
					this.job_type = "W";
					this.level = 3;
					this.explain = "ユグドラシル全土の飢えをなくすため<br>アースガルド農場化政策を打ち出す。<br>精霊王になりアースガルドの天候権、大地権を手に入れたい。";
					break;
				case 14:
					this.id = 14;
					this.name = "愛の女神フレイヤ";
					this.type = 1;
					this.default_list = [
						["K",  "K",  "Tr",  "W",  "W"],
						["K",  "K",  "Tr",  "W",  "W"],
						["G",  "Br",  "Br",  "G",  "G"]
					];
					this.patterns = [
						[
							["K",  "K",  "Tr",  "W",  "W"],
							["K",  "K",  "Tr",  "W",  "W"],
							["G",  "Br",  "Br",  "G",  "G"]
						],
						[
							["K",  "K",  "Br",  "Br",  "K"],
							["W",  "W",  "W",  "W",  "K"],
							["Tr",  "G",  "G",  "G",  "Tr"]
						],
						[
							["W",  "W",  "Br",  "Br",  "W"],
							["Tr",  "K",  "K",  "K",  "W"],
							["K",  "G",  "G",  "G",  "Tr"]
						]
					];
					this.job_type = "Tr";
					this.level = 3;
					this.explain = "生まれた国ヴァナヘイム、争いの絶えないヨトゥンヘイム。<br>アガペーの力で両国の和平条約取り決めを進めるため、<br>アースガルズ初の女神精霊王を目指す。";
					break;
				case 15:
					this.id = 15;
					this.name = "神の災厄フェンリル";
					this.type = 1;
					this.default_list = [
						["Bs",  "",  "Bs",  "",  "Bs"],
						["",  "",  "",  "",  ""],
						["Bs",  "",  "Bs",  "",  "Bs"]
					];
					this.job_type = "Bs";
					this.level = 3;
					this.explain = "思うがままを欲するフェンリル。<br>法も社会も何も縛るもののない無秩序を目指す。<br>それこそが真の自由なのだ。";
					break;
				default :
					return false;
			}

		}
	};

	return self;
}

/*
 * * * * * * * * * * * * * * * * * [list sample]
	["",  "",  "",  "",  ""],
	["",  "",  "",  "",  ""],
	["",  "",  "",  "",  ""]
 * * * * * * * * * * * * * * * * * 
*/
