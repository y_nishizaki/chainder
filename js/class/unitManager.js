/**
 * unitManager.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var UnitManager = new function() {
	var units;

	var self = function UnitManager(own_units) {
		// init units
		this.units = new Array();

		// own units is associative array 
		//   own_units = { "Knight": 9, "Guardian" : 9, "Wizard" : 9 }
		if (typeof own_units === 'undefined') own_units = new Array();

		// units is made by own_units
		for (var key in own_units) {
			var value = own_units[key];
			for (var i=0; i<value; i++) {
				this.addUnit(key);
			}
		}
	}

	self.prototype = {
		constructor: self
		/**
		 * getter method
		 */
		, getUnits: function() {
			return this.units;
		}
		, getLastUnit: function() {
			var index = this.units.length - 1;
			var last_unit = this.units[index];
			if (last_unit) {
				return last_unit;
			}
			return false;
		}

		/**
		 * unit manager
		 */
		, addUnit: function(job_type) {
			var unit = new Unit(job_type);
			this.units.push(unit);
			return unit;
		}
		, removeUnit: function(job_type) {
			var unit = new Unit(job_type);
			var index;
			for (var i=0; i<this.units.length; i++) {
				if (this.units[i].job.getType() == unit.job.getType()) {
					index = i;
				}
			}
			if (typeof index !== 'undefined') {
				return this.units.splice(index, 1);
			}
			return false;
		}

		/**
		 * count specified job unit
		 */
		, countUnit: function(job_name) {
			// 引数が指定されていない場合はユニット全体の数を返す
			if (typeof job_name === 'undefined') return this.units.length;

			var cnt = 0;
			var target_job = new Job(job_name);
			// 引数と同じジョブのユニットをカウントアップする
			for (var i=0; i<this.units.length; i++) {
				if (this.units[i].job.getType() == target_job.getType()) {
					cnt++;	
				}
			}
			return cnt;
		}

		/**
		 * discard units
		 */
		, vacate: function() {
			this.units = new Array();
		}
		
		/**
		 * debug method
		 */
		 , debugState: function() {
			var str = "";
			str += 'Kx'	+ this.countUnit("Knight");
			str += ', Gx' + this.countUnit("Guardian");
			str += ', Wx'	+ this.countUnit("Wizard");
			str += ', Brx'	+ this.countUnit("Bridge");
			str += ', Trx'	+ this.countUnit("Terminator");
			str += ', Bsx'	+ this.countUnit("Berserker");
			console.log(str);
		 } 
	};

	return self;
}


