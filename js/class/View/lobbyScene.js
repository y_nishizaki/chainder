/**
 * lobbyScene.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var L_CHALLENGER_WIDTH		= 290;
var L_CHALLENGER_HEIGHT		= 230;
var L_CHALLENGER_COLS		= 10;
var L_BTN_WIDTH				= 69;
var L_BTN_HEIGHT			= 39;

var LobbyScene = Class.create(Scene,{
	initialize: function(model) {
		Scene.call(this, arguments);

		// Common Settings
		this.backgroundColor = 'white';
		if (typeof model == 'undefined') model = "";
		this.model = model;

		// BackGround
		this.bg = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		this.bg.image = Game.Seq.core.assets['images/loa/lobby.png'];
		this.bg.frame = 0;
		this.addChild(this.bg);

		// Challenger Table
		this.challengerTable = new ChallengerTable();
		this.challengerTable.x = 15;
		this.challengerTable.y = 60;
		this.addChild(this.challengerTable);

		// Introduction Table
		this.introductionTable = new IntroductionTable();
		this.introductionTable.x = 0;
		this.introductionTable.y = 50;
		this.addChild(this.introductionTable);
		this.introductionTable.hide();

		// 次へ
		this.btn_next = new SpriteBase(L_BTN_WIDTH, L_BTN_HEIGHT);
		this.btn_next.image = Game.Seq.core.assets['images/loa/btn_next.png'];
		this.btn_next.x = 240;
		this.btn_next.y = 277;
		this.btn_next.scaleX = 0.9;
		this.btn_next.scaleY = 0.9;
		this.addChild(this.btn_next);
		// GameClearした場合に表示する
		this.btn_next.visible = false;

		// Event Settings 
		this.btn_next.addEventListener(enchant.Event.TOUCH_START, function(e){
			Game.Seq.lobbyController.showGameClear();
		});

		// FPS viewer
		if (Game.Debug) {
			this.fps = new FPS(30);
			this.label_fps = new Label();
			this.label_fps.x = 15;
			this.label_fps.y = 15;
			this.label_fps.text = "test fps";
			this.addChild(this.label_fps);
			this.on('enterframe', this.countFps);
		}
	}
	, countFps: function(e) {
		this.fps.check();
		this.label_fps.text = this.fps.getFPS();
	}

	/**
	 * Draw Congratulations!
	 */
	, appearCongratulations: function() {
		// Group
		this.cutin = new Group();
		// BackGround
		var bg = new Sprite(GAME_WIDTH, 40);
		bg.backgroundColor = 'rgba(255,255,255,0.8)';
		bg.y	= 0;
		// Congratulations
		var cong = new Sprite(280, 35);
		cong.image = Game.Seq.core.assets['images/loa/congratulations.png'];
		cong.x = 15;
		cong.y = 5;
		// Add Scene
		this.cutin.addChild(bg);
		this.cutin.addChild(cong);
		this.cutin.x = 0;
		this.cutin.y = (GAME_HEIGHT / 2) - bg.height;
		this.addChild(this.cutin);
	}
	, removeCutIn: function() {
		if (this.cutin) {
			this.removeChild(this.cutin);
			this.cutin = null;
		}
	}

});

/**
 * Challenger Table
 */
var ChallengerTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		var clickmap = new Sprite(L_CHALLENGER_WIDTH, L_CHALLENGER_HEIGHT);
		//  var surface	 = new Surface(L_CHALLENGER_WIDTH, L_CHALLENGER_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth	= 1;
		//  surface.context.lineJoin	= "round";
		//  surface.context.strokeRect(0, 0, L_CHALLENGER_WIDTH, L_CHALLENGER_HEIGHT);
		//  clickmap.image  = surface;	
		this.addChild(clickmap);

		// 遊び方
		this.btn_manual = new Sprite(L_BTN_WIDTH, L_BTN_HEIGHT);
		this.btn_manual.image = Game.Seq.core.assets['images/loa/btn_manual.png'];
		this.btn_manual.x = 17;
		this.btn_manual.y = 221;
		this.btn_manual.scaleX = 0.9;
		this.btn_manual.scaleY = 0.9;
		this.btn_manual.originX = 0;
		this.btn_manual.originY = 0;
		this.addChild(this.btn_manual);

		// 対戦相手をタッチしてください
		this.explain = new Label();
		this.explain.text = "対戦相手をタッチしてください";
		this.explain.font = "12px serif";
		this.explain.x = 100;
		this.explain.y = 230;
		this.addChild(this.explain);

		// Event Settings
		this.btn_manual.addEventListener(enchant.Event.TOUCH_START, function(e){
			Game.Seq.lobbyController.clickManualBtn();
		});

	}
	, initLabels: function() {
		// Challengers Label
		var challengers = this.scene.model.getChallengers();
		for (var i=0; i<challengers.length; i++) {
			var challenger = challengers[i];
			// initialize
			eval('this.label' + i + "= new Label()");
			// set name
			eval('this.label' + i).text = challenger.getName();
			// set id
			eval('this.label' + i).id = challenger.getId();
			// set color
			eval('this.label' + i).color = "black";
			// set font
			eval('this.label' + i).font = "12px serif";
			// add to scene
			this.addChild(eval('this.label' + i));
			// Event Settings
			eval('this.label' + i).addEventListener(enchant.Event.TOUCH_START, function(e){
				var id = this.id;
				Game.Seq.lobbyController.touchChallenger(id);
			});

			var x_index = Math.floor(i%5);			// 0,1,2,3,4
			var y_index = Math.floor( (i%5+1)/2);	// 0,1,1,2,2
			eval('this.label' + i).x = 150 - 140 * (x_index%2);
			eval('this.label' + i).y = 20 * y_index + 80 * Math.floor(i/5); 
		}

		// Level
		this.lv1 = new Label();
		this.lv1.text = "Level";
		this.lv1.font = "16px serif";
		this.lv1.x = 10;
		this.lv1.y = -3;
		this.addChild(this.lv1);

		this.crystal1 = new SpriteCrystal();
		this.crystal1.setLevel(1);
		this.crystal1.x = 50;
		this.crystal1.y = -10;
		this.addChild(this.crystal1);

		this.lv2 = new Label();
		this.lv2.text = "Level";
		this.lv2.font = "16px serif";
		this.lv2.x = 10;
		this.lv2.y = 77;
		this.addChild(this.lv2);

		this.crystal2 = new SpriteCrystal();
		this.crystal2.setLevel(2);
		this.crystal2.x = 50;
		this.crystal2.y = 70;
		this.addChild(this.crystal2);

		this.lv3 = new Label();
		this.lv3.text = "Level";
		this.lv3.font = "16px serif";
		this.lv3.x = 10;
		this.lv3.y = 157;
		this.addChild(this.lv3);

		this.crystal3 = new SpriteCrystal();
		this.crystal3.setLevel(3);
		this.crystal3.x = 50;
		this.crystal3.y = 150;
		this.addChild(this.crystal3);

	}
	, setKnockDown: function(id) {
		var challengers = this.scene.model.getChallengers();
		for (var i=0; i<challengers.length; i++) {
			// ラベルを選択
			var label = eval('this.label' + i);
			if (label.id == id) {
				// ラベル幅の取得
				var width  = label._boundWidth;	
				var height = 12;
				// 打ち消し線の描画
				var sprite = new Sprite(width, height);
				var surface = new Surface(width, height);
				surface.context.strokeStyle = "black";
				surface.context.beginPath();
				surface.context.moveTo(0,height/2);
				surface.context.lineTo(width,height/2);
				surface.context.stroke();
				surface.context.closePath();
				sprite.image = surface;
				// スプライト位置
				sprite.x = label.x;
				sprite.y = label.y;
				// シーンに追加
				this.addChild(sprite);
			}
		}
	}
	, show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = true;
		}
		this.crystal1.show();
		this.crystal2.show();
		this.crystal3.show();
	}
	, hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = false;
		}
		this.crystal1.hide();
		this.crystal2.hide();
		this.crystal3.hide();
	}
});


/**
 * Introduction Table
 */
var IntroductionTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		var clickmap = new Sprite(GAME_WIDTH, L_CHALLENGER_HEIGHT);
		//  var surface	 = new Surface(GAME_WIDTH, L_CHALLENGER_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth	= 1;
		//  surface.context.lineJoin	= "round";
		//  surface.context.strokeRect(0, 0, GAME_WIDTH, L_CHALLENGER_HEIGHT);
		//  clickmap.image  = surface;	
		this.addChild(clickmap);

		// Foramtion Frame
		this.frame = new Sprite(302, 182);
		this.frame.image = Game.Seq.core.assets['images/loa/formation_frame.png'];
		this.frame.frame = 0;
		this.frame.x = 70;
		this.frame.y = 80;
		this.frame.scaleX = 0.61;
		this.frame.scaleY = 0.61;
		this.frame.originX = 0;
		this.frame.originY = 0;
		this.addChild(this.frame);

	}
	, set: function(challenger) {
		// Challenger
		//  var challengers = this.scene.model.getChallengers();
		//  var challenger = challengers[12];
		if (typeof challenger === 'undefined')	return false;

		// Name
		this.name = new Label();
		this.name.text = challenger.getName();
		this.name.font = "16px serif";
		this.name.x = (GAME_WIDTH / 2) - (this.name._boundWidth /2);
		this.name.y = 0;
		this.addChild(this.name);
		// Explain
		this.explain = new Label();
		this.explain.text = challenger.getExplain();
		this.explain.font = "10px serif";
		this.explain.textAlign = "center";
		this.explain.x = 10;
		this.explain.y = 30;
		this.addChild(this.explain);
		// Job Type
		this.job_type = new Label();
		this.job_type.text = "Type:";
		this.job_type.font = "16px serif";
		this.job_type.x = 70;
		this.job_type.y = 200;
		this.addChild(this.job_type);
		// Battle Point
		this.battle_point = new Label();
		this.battle_point.text = "戦闘力:" + challenger.getBattlePoint();
		this.battle_point.font = "16px serif";
		this.battle_point.x = 170;
		this.battle_point.y = 200;
		this.addChild(this.battle_point);
		// Level
		var level = challenger.getLevel();
		this.crystal = new SpriteCrystal();
		this.crystal.setLevel(level);
		this.crystal.x = (GAME_WIDTH / 2) + (this.name._boundWidth / 2);
		this.crystal.y = -5;
		this.addChild(this.crystal);
		// Formation
		this.formation = new Group();
		this.formation.x = 75;
		this.formation.y = 82;
		this.addChild(this.formation);
		// Make Units
		var list = challenger.getList();
		var arrangement = new Arrangement(list);
		var units = arrangement.getUnits();
		for (var i=0; i<units.length; i++) {
			var unit = units[i];
			if (unit) {
				var sprite  = new SpriteBattler(UNIT_WIDTH, UNIT_HEIGHT);
				sprite.setUnit(unit);
				sprite.setPos();
				this.formation.addChild(sprite);
			}
		} 
		// Type Elements
		var job_type  = challenger.getJobType();
		this.type_unit = new Unit(job_type);
		this.element   = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element.x = 120;
		this.element.y = 192;
		this.element.clearSurface();
		this.element.setUnit(this.type_unit);
		this.addChild(this.element);

		// Back Button
		this.btn_back = new Sprite(L_BTN_WIDTH, L_BTN_HEIGHT);
		this.btn_back.image = Game.Seq.core.assets['images/loa/btn_back.png'];
		this.btn_back.x = 32;
		this.btn_back.y = 231;
		this.btn_back.scaleX = 0.9;
		this.btn_back.scaleY = 0.9;
		this.btn_back.originX = 0;
		this.btn_back.originY = 0;
		this.addChild(this.btn_back);

		// 次へ
		this.btn_next = new SpriteBase(L_BTN_WIDTH, L_BTN_HEIGHT);
		this.btn_next.image = Game.Seq.core.assets['images/loa/btn_next.png'];
		this.btn_next.x = 220;
		this.btn_next.y = 227;
		this.btn_next.scaleX = 0.9;
		this.btn_next.scaleY = 0.9;
		this.addChild(this.btn_next);
		this.btn_next.flushOn();

		// ID
		this.id = challenger.getId();

		// Event Settings
		this.btn_next.addEventListener(enchant.Event.TOUCH_START, function(e){
			var id = this.parentNode.id;
			Game.Seq.lobbyController.touchIntroduction(id);
		});
		this.btn_back.addEventListener(enchant.Event.TOUCH_START, function(e){
			Game.Seq.lobbyController.touchBack();
		});
	}
	, unset: function() {
		this.removeChild(this.name);
		this.removeChild(this.explain);
		this.removeChild(this.job_type);
		this.removeChild(this.battle_point);
		this.removeChild(this.crystal);
		this.removeChild(this.formation);
		this.removeChild(this.element);
		this.removeChild(this.btn_back);
		this.removeEventListener('touchstart');
	}
	, getDisplayType: function(battler) {
		if ("str" == battler.getType()) {
			return 'Knight';
		} else if ("def" == battler.getType()) {
			return 'Guardian';
		} else if ("mgc" == battler.getType()) {
			return 'Wizard';
		}
	}
	, show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = true;
		}
		// btn_next 点滅ON
		if (this.btn_next) {
			this.btn_next.flushOn();
		}

	}
	, hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = false;
		}
		// btn_next 点滅OFF
		if (this.btn_next) {
			this.btn_next.flushOff();
		}
	}
});

