/**
 * spriteBattler.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteBattler = Class.create(SpriteBase,{
	initialize: function(width, height) {
		SpriteBase.call(this, width, height);

		// Common Settings
		this.name = "SpriteBattler";
		this.unit;

	}

	, setUnit: function(unit) {
		SpriteBase.prototype.setUnit.call(this, unit);
		this.unit = unit;
	}
	, setPos: function() {
		// battle.formation$B$NJB$S=g$O:8(B90$BEY2sE>(B
		//  var reverseRowIndex  = (FORMATION_ROWS -1) - this.unit.rowIndex;
		//  this.x = this.unit.lineIndex * 50;
		//  this.y = reverseRowIndex * 50;
		// battle.formation$B$NJB$SJ}$ODL>o(B
		this.x = this.unit.rowIndex * 36;
		this.y = this.unit.lineIndex * 36;
	}
	, update: function(unit) {
		SpriteBase.prototype.update.call(this);
		//this.setPos();
	}
	
});


