/**
 * spriteBase.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteBase = Class.create(Sprite,{
	initialize: function(width, height) {
		Sprite.call(this, width, height);

		// Common Settings
		this.surface			= new AdvancedSurface(width, height);
		this._mirror			= false;
		this._blink				= false;
		this._blink_count		= 0;
		this._flash				= false;
		this._flash_count		= 0;
		this._collapse_duration = 0;
		this._damage_duration	= 0;
		this._damage_sprite;
		this._drag				= false;
		this._select			= false;

		// Event Settings
		this.on('enterframe', this.update);

		// Surface Settings
		this.image = this.surface;
	}

	/**
	 * Update (called in Enterframe event)
	 */
	, update: function() {
		this.surface.update();
		this.adaptMirror();
		this.adaptBlink();
		this.adaptFlush();
		this.adaptCollapse();
		this.adaptDamage();
	}
	, setUpdate: function() {
		this.on("enterframe", this.update);
	}
	, unsetUpdate: function() {
		this.removeEventListener("enterframe", this.update);
	}

	/**
	 * Surface Manager
	 */
	, clearSurface: function() {
		this.surface.context.clearRect(0,0,this.width, this.height);
	}
	
	/**
	 * Mirror manages reversal state
	 */
	, isMirror: function() {
		return this._mirror;
	}
	, mirrorOn: function() {
		this._mirror = true;
		this.update();
	}
	, mirrorOff: function() {
		this._mirror = false;
		this.update();
	}
	, adaptMirror: function() {
		if (this.isMirror()) {
			if (this.scaleX > 0) {
				this.scaleX *= -1;
			}
		} else {
			if (this.scaleX < 0) {
				this.scaleX *= -1;
			}
		}
	}

	/**
	 * Blink state
	 */
	, isBlink: function() {
		return this._blink;
	}
	, blinkOn: function() {
		this._blink = true;
		this._blink_count = 0;
	}
	, blinkOff: function() {
		this._blink = false;
	}
	, adaptBlink: function() {
		if (this.isBlink()) {
			var alpha;
			this._blink_count = (this._blink_count + 1) % 32;
			if (this._blink_count < 16) {
				alpha = (16 - this._blink_count) * 6;
			} else {
				alpha = (this._blink_count - 16) * 6;
			}
			alpha = alpha / 255;
			this.surface.addBlink(alpha);
		}
	}

	/**
	 * Flush state
	 */
	, isFlush: function() {
		return this._flush;
	}
	, flushOn: function() {
		this._flush = true;
		this._flush_count = 0;
	}
	, flushOff: function() {
		this._flush = false;
	}
	, adaptFlush: function() {
		if (this.isFlush()) {
			this._flush_count = (this._flush_count + 1) % 40; 
			if (this._flush_count < 30) {
				this.visible = true;
			} else {
				this.visible = false;
			}
		}
	}

	/**
	 * Collapse emit red flash (Mostly used when Character receive damage)
	 */
	, collapse: function() {
		this._collapse_duration = 10;
	}
	, adaptCollapse: function() {
		if (this._collapse_duration > 0 ) {
			this._collapse_duration -= 1;
			this.opacity = (100 - (10 - this._collapse_duration) * 10) / 100;
			this.surface.addCollapse();
			// collapse end
			if (this._collapse_duration == 0) {
				this.visible = false;
			}
		}
	}

	/**
	 * Damage draw value
	 */
	, damage: function(value, critical) {
		  this._damage_duration = 40;
		  this._damage_value	= value;
		  this._damage_critical = critical;
		  // Effect Sprite
		  this._damage_sprite = new SpriteBase(50,50);
		  this._damage_sprite.name = 'damage sprite';
		  this._damage_sprite.x = this.x;
		  this._damage_sprite.y = this.y;
		  this.scene.addChild(this._damage_sprite);
	}
	, adaptDamage: function() {
		if (this._damage_duration > 0 ) {
			this._damage_duration -= 1;
			
			// animation damage sprite
			if (this._damage_duration >= 38) {
			  this._damage_sprite.y -= 4;
			} else if (this._damage_duration >= 36) {
			  this._damage_sprite.y -= 2;
			} else if (this._damage_duration >= 34) {
			  this._damage_sprite.y += 2;
			} else if (this._damage_duration >= 28) {
			  this._damage_sprite.y += 4;
			}
			// draw damage
			this._damage_sprite.opacity = (256 - (12 - this._damage_duration) * 32) / 256;
			this._damage_sprite.surface.addDamage(this._damage_value, this._damage_critical);

			// terminate effect
			if (this._damage_sprite.opacity < 0) {
			  this._damage_sprite.visible = false;
			}
			if (this._damage_duration == 0) {
			  this.disposeDamage();
			}
		}
	}
	, disposeDamage: function() {
		// remove from scene
		this.scene.removeChild(this._damage_sprite);
		// memory dispose
		delete this._damage_sprite;
	}

	/**
	 * Set Unit
	 */
	, setUnit: function(unit) {
		this.surface.makeUnit(unit);
		this.image = this.surface;
	}

	/**
	 * Drag
	 */
	, isDragable: function() {
		return this._drag;
	}
	, dragStart: function(e){
		this.originX = e.x - this.x;
		this.originY = e.y - this.y;
	}
	, dragMove: function(e){
		this.x = e.x - this.originX;
		this.y = e.y - this.originY;
	}
	, dragOn: function() {
		this._drag = true;
		this.addEventListener(enchant.Event.TOUCH_START, this.dragStart);
		this.addEventListener(enchant.Event.TOUCH_MOVE, this.dragMove);
	}
	, dragOff: function() {
		this._drag = false;
		this.removeEventListener(enchant.Event.TOUCH_START, this.dragStart);
		this.removeEventListener(enchant.Event.TOUCH_MOVE, this.dragMove);
	}

	/**
	 * Select state
	 */
	, turnSelected: function(e) {
		if (this.isSelected()) {
			this.selectOff();
		} else {
			this.selectOn();
		}
	}
	, isSelected: function() {
		return this._select;
	}
	, selectOn: function() {
		this._select = true;
		this.blinkOn();
	}
	, selectOff: function() {
		this._select = false;
		this.blinkOff();
	}

	/**
	 * Dispose
	 */
	, dispose: function() {
		this.tl.removeFromScene();
		if (this.parentNode) {
			this.parentNode.removeChild(this);
		}
	}

});

