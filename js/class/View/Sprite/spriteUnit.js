/**
 * spriteUnit.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteUnit = Class.create(SpriteBase,{
	initialize: function(width, height) {
		SpriteBase.call(this, width, height);

		// Common Settings
		this.name = "SpriteUnit";
		this.unit;
	}

	/**
	 * setter
	 */
	, setUnit: function(unit) {
		SpriteBase.prototype.setUnit.call(this, unit);
		this.unit = unit;
	}

	/**
	 * position update
	 */
	, setPos: function() {
		this.x = this.unit.rowIndex * 60;
		this.y = this.unit.lineIndex * 60;
	}

	/**
	 * update
	 */
	, update: function(unit) {
		SpriteBase.prototype.update.call(this);
	}
	
});

