/**
 * spriteDamage.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteDamage = Class.create(Group,{
	initialize: function(value, damage_type) {
		var width  = 90;
		var height = 30;
		// Parent Call
		Group.call(this, width, height);

		// Sprites Settings
		this.sprites = new Array();

		this.setDamage(value, damage_type);
	}

	/**
	 * Damage Value
	 */
	, setDamage: function(value, damage_type) {
		// ダメージの値を文字列に変換
		value = value.toString();
		for (var i=0; i<value.length; i++) {
			var num		 = value.charAt(i);
			var critical = false;
			// Criticalの判別
			if (damage_type == 2) {
				critical = true;
			}
			var sprite = new SpriteNumber(num, critical);
			// SpriteDamageにaddChildする
			this.addChild(sprite);
			// 桁数に応じてx座標を変更する
			sprite.x = 30 * i;
		}
		this.setDisposeEvent();
	}

	, setDisposeEvent: function() {
		this.tl.cue({
			30: function(){ 
				this.parentNode.removeChild(this);
			},
		});
	}

});


