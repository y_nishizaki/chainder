/**
 * spriteGauge.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SpriteGauge = Class.create(Sprite,{
	initialize: function(width, height) {
		Sprite.call(this, width, height);

		// Common Settings
		this.surface			= new Surface(width, height);
		this.type	 = 0;	// 0:surface, 1:image
		this.scale_x = 1.0;	
		this.frame	= true;	// surface frame
		this.frameColor	= "black";
		this.fillColor	= "red";

		// Surface Settings
		this.image = this.surface;
	}

	/**
	 * Scale Manager 
	 */
	, setScale: function(value, max) {
		this.scale_x	= Math.floor(value / max * 100) / 100;
	}

	/**
	 * Draw Surface
	 */
	, draw: function() {
		// clear
		this.surface.context.clearRect(0, 0, this.width, this.height);
		// fill
		this.drawGauge();
		// frame
		this.drawFrame();
	}
	, drawGauge: function() {
		this.surface.context.fillStyle = this.fillColor;
		this.surface.context.fillRect(0, 0, this.width * this.scale_x, this.height);
	}
	, drawFrame: function() {
		if (this.frame) {
			this.surface.context.strokeStyle = this.frameColor;
			this.surface.context.lineWidth	= 2;
			this.surface.context.lineJoin	= "round";
			this.surface.context.strokeRect(0, 0, this.width, this.height);
		}
	}

	/**
	 * Image Manager
	 */
	, setImage: function(src) {
	}

});

