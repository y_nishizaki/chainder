/**
 * battleScene.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var B_DISPLAY_WIDTH		= 300;
var B_DISPLAY_HEIGHT	= 45;
var B_FORMATION_WIDTH	= 150;
var B_FORMATION_HEIGHT	= 250;
var B_BLOCK_WIDTH		= 50;
var B_BLOCK_HEIGHT		= 50;
var B_HPGAUGE_WIDTH		= 100;
var B_HPGAUGE_HEIGHT	= 5;

var BattleScene = Class.create(Scene,{
	initialize: function(model) {
		Scene.call(this, arguments);

		// Common Settings
		this.backgroundColor = 'white';
		if (typeof model == 'undefined') model = "";
		this.model = model;
		this.sprites = new Array();;
		this.sprite;

		// BackGround
		this.bg = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		this.bg.image = Game.Seq.core.assets['images/loa/bg01.png'];
		this.bg.frame = 0;
		this.addChild(this.bg);

		// Frame
		this.frame = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		this.frame.image = Game.Seq.core.assets['images/loa/battle.png'];
		this.frame.frame = 0;
		this.addChild(this.frame);
		
		// Friends Table
		this.friendsTable = new BattleFormationTable();
		this.friendsTable.x = 132;
		this.friendsTable.y = 192;
		this.addChild(this.friendsTable);

		// Enemy Table
		this.enemyTable = new BattleFormationTable();
		this.enemyTable.x = 12;
		this.enemyTable.y = 23;
		this.addChild(this.enemyTable);

		// Battle Display Table
		this.battleDisplay = new BattleDisplayTable();
		this.battleDisplay.x = 10;
		this.battleDisplay.y = 0;
		this.addChild(this.battleDisplay);
		this.battleDisplay.update();

		// Initialize
		//  console.log("Game.Seq", Game.Seq.battleController);
		//  Game.Seq.battleController.setFriendsSprites();

		// Event Settings
		this.on('enterframe', this.update);

		// FPS viewer
		if (Game.Debug) {
			this.fps = new FPS(30);
			this.label_fps = new Label();
			this.label_fps.x = 15;
			this.label_fps.y = 300;
			this.label_fps.text = "test fps";
			this.addChild(this.label_fps);
			this.on('enterframe', this.countFps);
		}

	}
	, countFps: function(e) {
		this.fps.check();
		this.label_fps.text = this.fps.getFPS();
	}

	/**
	 * update
	 */
	, update: function() {
		// Battleのメインループ
		Game.Seq.battleController.enterFrameFunc();
	}

	/**
	 * Draw Battle Result 
	 */
	, appearTransferTactics: function() {
		var type = Game.Seq.battle.enemy.getType();	
		// 敵の弱点分析
		var weak = "";
		switch (type) {
			case 'str':
				weak = "水";
				break;
			case 'def':
				weak = "雷";
				break;
			case 'mgc':
				weak = "火";
				break;
		}
		this.setCutInLabel("Go To Tactics Phase!<br>相手は" + weak + "が弱点だ！");
		this.addNextCutIn();
	}
	, appearWin: function() {
		this.setCutInLabel("You Win!<br>画面をタッチしてください.");
	}
	, appearLose: function() {
		this.setCutInLabel("You Lose...<br>画面をタッチしてください.");
	}
	, appearDrawGame: function() {
		this.setCutInLabel("Draw Game<br>画面をタッチしてください.");
	}
	, setCutInLabel: function(str) {
		if (typeof str === 'undefined') str = "";
		// Group
		this.cutin = new Group();
		this.cutin.x = 0;
		this.cutin.y = 0;
		// Clickmap
		var clickmap = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		// BackGround
		var bg = new Sprite(GAME_WIDTH, 40);
		bg.backgroundColor = 'rgba(255,255,255,0.8)';
		bg.y	= (GAME_HEIGHT / 2) - (bg.height / 2);
		// Label
		var label = new Label();
		label.text = str;
		label.textAlign = "center";
		label.x	= 10
		label.y	= (GAME_HEIGHT / 2) - (bg.height / 2) + 5;
		// Event Settings
		this.cutin.addEventListener(enchant.Event.TOUCH_START, function(e){
			Game.Seq.battleController.touchCutIn(e);
		});
		// Add Scene
		this.cutin.addChild(clickmap);
		this.cutin.addChild(bg);
		this.cutin.addChild(label);
		this.addChild(this.cutin);
	}
	, removeCutIn: function() {
		if (this.cutin) {
			this.removeChild(this.cutin);
			this.cutin = null;
		}
	}
	, addNextCutIn: function() {
		if (this.cutin) {
			// 次へ
			this.cutin.btn_next = new SpriteBase(L_BTN_WIDTH, L_BTN_HEIGHT);
			this.cutin.btn_next.image = Game.Seq.core.assets['images/loa/btn_next.png'];
			this.cutin.btn_next.x = 240;
			this.cutin.btn_next.y = (GAME_HEIGHT / 2) - 15;
			this.cutin.btn_next.scaleX = 0.6;
			this.cutin.btn_next.scaleY = 0.6;
			this.cutin.btn_next.flushOn();
			this.cutin.addChild(this.cutin.btn_next);
		}
	}

	/**
	 * Attack Effect
	 */
	, addAttackEffect: function() {
		// Battler 
		var enemy   = this.scene.model.enemy;
		var friends = this.scene.model.friends;
		// Attack Friends
		var f_effect	= new SpriteEffect();
		f_effect.x = 105;
		f_effect.y = 150;
		f_effect.attackEffect(enemy);
		// Attack Enemy
		var e_effect	= new SpriteEffect();
		e_effect.x = 5;
		e_effect.y = 0;
		e_effect.scaleX = -1;
		e_effect.attackEffect(friends);
		// on Scene
		this.scene.addChild(f_effect);
		this.scene.addChild(e_effect);
	}
});


/**
 * Battle Formation Table
 */
var BattleFormationTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		//  var clickmap = new Sprite(B_FORMATION_WIDTH, B_FORMATION_HEIGHT);
		//  var surface	 = new Surface(B_FORMATION_WIDTH, B_FORMATION_HEIGHT);
		//  surface.context.strokeStyle = "rgba(255,255,255,0.2)";
		//  surface.context.lineWidth	= 1;
		//  surface.context.lineJoin	= "round";

		//  // Formation Blocks
		//  var tx, ty = 0;
		//  for (var i=0; i<FORMATION_COLS; i++) {
		//  	for (var j=0; j<FORMATION_ROWS; j++) {
		//  		tx = i * B_BLOCK_WIDTH;
		//  		ty = j * B_BLOCK_HEIGHT;
		//  		surface.context.strokeRect(tx, ty, B_BLOCK_WIDTH, B_BLOCK_HEIGHT);
		//  	}
		//  }
		//  surface.context.strokeRect(0, 0, B_FORMATION_WIDTH, B_FORMATION_HEIGHT);
		//  clickmap.image  = surface;	
		//  this.addChild(clickmap);

	}
});


/**
 * Battle Display Table
 */
var BattleDisplayTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		//  var clickmap = new Sprite(B_DISPLAY_WIDTH, B_DISPLAY_HEIGHT);
		//  var surface	 = new Surface(B_DISPLAY_WIDTH, B_DISPLAY_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth	= 2;
		//  surface.context.lineJoin	= "round";
		//  surface.context.strokeRect(0, 0, B_DISPLAY_WIDTH, B_DISPLAY_HEIGHT);
		//  clickmap.image  = surface;	
		//  this.addChild(clickmap);


		// Labels initialized
		this.label1_1 = new Label();
		this.label1_2 = new Label();
		this.label1_3 = new Label();
		this.label1_4 = new Label();
		this.label1_5 = new Label();
		this.label1_6 = new Label();
		this.label2_1 = new Label();
		this.label2_2 = new Label();
		this.label2_3 = new Label();
		this.label2_4 = new Label();
		this.label2_5 = new Label();
		this.label2_6 = new Label();

		// Labels setting
		this.label1_1.color = "white";
		this.label1_2.color = "white";
		this.label1_3.color = "white";
		this.label1_4.color = "white";
		this.label1_5.color = "white";
		this.label1_6.color = "white";
		this.label2_1.color = "white";
		this.label2_2.color = "white";
		this.label2_3.color = "white";
		this.label2_4.color = "white";
		this.label2_5.color = "white";
		this.label2_6.color = "white";

		this.label1_1.font = "10px serif";
		this.label1_2.font = "10px serif";
		this.label1_3.font = "10px serif";
		this.label1_4.font = "10px serif";
		this.label1_5.font = "10px serif";
		this.label1_6.font = "10px serif";
		this.label2_1.font = "10px serif";
		this.label2_2.font = "10px serif";
		this.label2_3.font = "10px serif";
		this.label2_4.font = "10px serif";
		this.label2_5.font = "10px serif";
		this.label2_6.font = "10px serif";

		this.label1_1.x = 0;
		this.label1_2.x = 0;
		this.label1_3.x = 0;
		this.label1_4.x = 0;
		this.label1_5.x = 45;
		this.label1_6.x = 0;
		this.label2_1.x =  200;
		this.label2_2.x =  200;
		this.label2_3.x =  200;
		this.label2_4.x =  200;
		this.label2_5.x =  245;
		this.label2_6.x =  200;
		
		this.label1_1.y = 200;
		this.label1_2.y = 215;
		this.label1_3.y = 245;
		this.label1_4.y = 268;
		this.label1_5.y = 268;
		this.label1_6.y = 280;
		this.label2_1.y = 30;
		this.label2_2.y = 45;
		this.label2_3.y = 75;
		this.label2_4.y = 98;
		this.label2_5.y = 98;
		this.label2_6.y = 110;

		this.label1_3.text = "type";
		this.label2_3.text = "type";

		this.addChild(this.label1_1);
		this.addChild(this.label1_2);
		this.addChild(this.label1_3);
		this.addChild(this.label1_4);
		this.addChild(this.label1_5);
		this.addChild(this.label1_6);
		this.addChild(this.label2_1);
		this.addChild(this.label2_2);
		this.addChild(this.label2_3);
		this.addChild(this.label2_4);
		this.addChild(this.label2_5);
		this.addChild(this.label2_6);

		// Element Settings
		this.element1 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element2 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element1.x = 35;
		this.element1.y = 238;
		this.element2.x = 233;
		this.element2.y = 67;
		this.unit1 = new Unit("Knight");
		this.unit2 = new Unit("Knight");
		this.element1.setUnit(this.unit1);
		this.element1.unsetUpdate();
		this.element2.setUnit(this.unit2);
		this.element2.unsetUpdate();
		this.addChild(this.element1);
		this.addChild(this.element2);

		// HP Gauge
		this.hp_gauge_f = new SpriteGauge(B_HPGAUGE_WIDTH, B_HPGAUGE_HEIGHT);
		this.hp_gauge_f.x = 0;
		this.hp_gauge_f.y = 230;

		this.hp_gauge_e = new SpriteGauge(B_HPGAUGE_WIDTH, B_HPGAUGE_HEIGHT);
		this.hp_gauge_e.x = 200;
		this.hp_gauge_e.y = 60;

		this.addChild(this.hp_gauge_f);
		this.addChild(this.hp_gauge_e);

	}
	, update: function() {
		this.drawType();
		this.drawHp();
		this.drawStatus();
	}
	, drawType: function() {
		// Battler
		var friends = this.scene.model.friends;
		var enemy   = this.scene.model.enemy;
		// Friends
		var type_friends = friends.getJobType();
		this.unit1.changeJob(type_friends);
		this.element1.clearSurface();
		this.element1.setUnit(this.unit1);
		// Enemy
		var type_enemy = enemy.getJobType();
		this.unit2.changeJob(type_enemy);
		this.element2.clearSurface();
		this.element2.setUnit(this.unit2);
	}
	, drawHp: function() {
		// Battler
		var friends = this.scene.model.friends;
		var enemy   = this.scene.model.enemy;
		// Friends
		this.label1_1.text = "オーディン";
		this.label1_2.text = 'HP ' + friends.getHp() + '/' + friends.getMaxHp(); 

		// Enemy
		this.label2_1.text = enemy.getBattlerName();
		this.label2_2.text = 'HP ' + enemy.getHp() + '/' + enemy.getMaxHp(); 

		// HP Gauge
		this.hp_gauge_f.setScale(friends.getHp(), friends.getMaxHp());
		this.hp_gauge_f.draw();
		this.hp_gauge_e.setScale(enemy.getHp(), enemy.getMaxHp());
		this.hp_gauge_e.draw();
	}
	, drawStatus: function() {
		// Battler
		var friends = this.scene.model.friends;
		var enemy   = this.scene.model.enemy;
		// Friends
		this.label1_4.text = 'ATK:' + friends.getAtkVal();
		this.label1_5.text = 'DEF:' + friends.getDefVal(enemy);
		this.label1_6.text = 'CRT:' + friends.getCrt();
		// Enemy
		this.label2_4.text = 'ATK:' + enemy.getAtkVal();
		this.label2_5.text = 'DEF:' + enemy.getDefVal(friends);
		this.label2_6.text = 'CRT:' + enemy.getCrt();
	}
});


