/**
 * advancedSurface.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var AdvancedSurface = Class.create(Surface,{
	initialize: function(width, height) {
		Surface.call(this, width, height);

		// Common Settings
		this.src			= "";
		this.frame			= 0;

	}
	/**
	 * Set Image
	 */
	, makeUnit: function(unit) {
		if (typeof unit === 'undefined')	return false;
		var src		= unit.job.getImageSrc();
		if (src) {
			// unitのJobから画像とフレームを所得
			this.src	= Game.Seq.core.assets[src];
			this.frame	= unit.job.getImageFrame();
			//this.context.clearRect(0,0,this.width, this.height);
			// draw
			this.drawUnit();
		} 
		return false;
	}
	, makeMonster: function(monster) {
	}
	, makeCursor: function(cursor) {
	}

	/**
	 * Draw
	 */
	, drawUnit: function() {
		if (typeof this.src == '') return false;
		// srcを(0, 0)に描画
		//  this.draw(src);
		// srcをトリミングして表示
		//  this.draw(sx, sy, sw, sh, dx, dy, dw, dh)に描画
		//  sx, sy, sw, sh : 切り抜く画像の座標と範囲
		//  dx, dy, dw, dh : Surface上に表示する座標と範囲
		// icon01.png
		//  var sx = w * this.frame;
		//  var sy = 0;
		// element.png
		var w = this.width;
		var h = this.height;
		var sx = w * (this.frame%3);
		var sy = h * Math.floor(this.frame/3);
		this.draw(this.src, sx, sy, w, h, 0, 0, w, h);
		// console
		//console.log("src", this.src);
		//console.log("frame", this.frame);
	}

	/**
	 * Update
	 */
	 , update: function() {
		var w = this.width;
		var h = this.height;
		if (this.src != "") {
			// draw
			this.drawUnit();
			// overlay mode
			this.context.globalCompositeOperation = "source-atop";
			//console.log("surface update draw");
		} else {
			this.context.clearRect(0, 0, w, h);
		}
	 }

	/**
	 * Effect
	 */
	 , addBlink: function(alpha) {
		this.context.globalCompositeOperation = "source-atop";
		//fillStyleをRGBAで指定
		//  this.context.fillStyle = "rgba(255,255,255,0.5)";
		this.context.fillStyle = "rgba(255,255,255," + alpha + ")";
		this.context.fillRect(0, 0, this.width, this.height);
	 }
	 , addCollapse: function() {
		this.context.globalCompositeOperation = "source-atop"; //"lighter";
		this.context.fillStyle = "rgba(255,64,64,0.3)";
		this.context.fillRect(0, 0, this.width, this.height);
	 }
	 , addDamage: function(value, crt) {
		this.context.globalCompositeOperation = "source-over";
		this.context.fillStyle = "red";
		this.context.font = "italic bold 2px sans-serif";
		this.context.fillText(String(value), 1, 12, 14);
	 }

});


