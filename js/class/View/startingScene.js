/**
 * startingScene.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var StartingScene = new function() {
	var title;
	var command1;

	var self = function StartingScene() {
		Scene.apply(this, arguments);

		// Common Settings
		this.backgroundColor = 'white';
		if (typeof model == 'undefined') model = "";
		this.model = model;

		// BackGround
		this.bg = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		this.bg.image = Game.Seq.core.assets['images/loa/title.png'];
		this.addChild(this.bg);

		var frame = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		var surface = new Surface(320, 320);
		surface.context.strokeStyle = "black";
		surface.context.lineWidth = 2;
		surface.context.strokeRect(0, 0, 320, 320);
		frame.image = surface;
		this.addChild(frame);

		// Commang Settings
		var command1 = new SpriteBase(161, 50);
		command1.image = Game.Seq.core.assets['images/loa/touch_start_mini.png'];
		command1.frame = [0,1,2,3]
		command1.x	= (GAME_WIDTH / 2) - (command1.width /2);
		command1.y	= 200;
		command1.flushOn();

		var command2 = new Label();
		command2.text = "Battle";
		command2.x	= 250;//(GAME_WIDTH / 2) - (command2._boundWidth /2);
		command2.y	= 170;

		var command3 = new Label();
		command3.text = "Debug";
		command3.x	= 250;//(GAME_WIDTH / 2) - (command3._boundWidth /2);
		command3.y	= 220;

		var command4 = new Label();
		command4.text = "Lobby";
		command4.x	= 250; //(GAME_WIDTH / 2) - (command4._boundWidth /2);
		command4.y	= 270;

		// Touch Start
		this.addChild(command1);

		// Debug Command
		if (Game.Debug) {
			this.addChild(command2);
			this.addChild(command3);
			this.addChild(command4);
		}
			
		// Event Settings
		this.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.startingController.NewGame();
		});
		command2.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.startingController.BattleTest();
		});
		command3.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.startingController.DebugTest();
		});
		command4.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.startingController.LobbyTest();
		});
	}

	var uber = enchant.Scene.prototype;

	self.prototype = extend(uber, {
		constructor: self
	});

	return self;
}

