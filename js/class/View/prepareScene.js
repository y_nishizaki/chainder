/**
 * prepareScene.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var P_DISPLAY_WIDTH		= 300;
var P_DISPLAY_HEIGHT	= 45;
var P_FORMATION_WIDTH	= 300;
var P_FORMATION_HEIGHT	= 180;
var P_BLOCK_WIDTH		= 59;
var P_BLOCK_HEIGHT		= 59;
var P_STOCK_WIDTH		= 300;
var P_STOCK_HEIGHT		= 60;
var S_LINE_WIDTH		= 18;
var S_LINE_HEIGHT		= 14;

var PrepareScene = Class.create(Scene,{
	initialize: function(model) {
		Scene.call(this, arguments);

		// Common Settings
		this.backgroundColor = 'white';
		if (typeof model == 'undefined') model = "";
		this.model = model;
		this.sprites = new Array();;
		this.sprite;
		
		// BackGround
		this.bg = new Sprite(GAME_WIDTH, GAME_HEIGHT);
		this.bg.image = Game.Seq.core.assets['images/loa/bg01.png'];
		this.bg.frame = 0;
		this.addChild(this.bg);

		// Frame
		this.frame = new Sprite(GAME_WIDTH, 51);
		this.frame.image = Game.Seq.core.assets['images/loa/prepare_status.png'];
		this.frame.frame = 0;
		this.addChild(this.frame);

		this.frame2 = new Sprite(302, 182);
		this.frame2.image = Game.Seq.core.assets['images/loa/formation_frame.png'];
		this.frame2.frame = 0;
		this.frame2.x = 9;
		this.frame2.y = 53;
		this.addChild(this.frame2);

		// Stock Table
		this.stockTable = new StockTable();
		this.stockTable.x = MARGIN_LEFT;
		this.stockTable.y = 250;
		this.addChild(this.stockTable);

		// Enemy Table
		this.enemyTable = new EnemyTable();
		this.enemyTable.x = MARGIN_LEFT;
		this.enemyTable.y = 250;
		this.addChild(this.enemyTable);
		// 戦闘中のみ表示する
		this.enemyTable.hide();

		// Formation Table
		this.formationTable = new FormationTable();
		this.formationTable.x = 13;
		this.formationTable.y = 55;
		this.addChild(this.formationTable);

		// Display Table
		this.displayTable = new DisplayTable();
		this.displayTable.x = MARGIN_LEFT;
		this.displayTable.y = 0;
		this.addChild(this.displayTable);
		this.displayTable.update();

		// FPS viewer
		if (Game.Debug) {
			this.fps = new FPS(30);
			this.label_fps = new Label();
			this.label_fps.x = 15;
			this.label_fps.y = 235;
			this.label_fps.text = "test fps";
			this.addChild(this.label_fps);
			this.on('enterframe', this.countFps);
		}
	}
	, countFps: function(e) {
		this.fps.check();
		this.label_fps.text = this.fps.getFPS();
	}
});


/**
 * Stock Table
 */
var StockTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// Button
		this.frame = new Sprite(GAME_WIDTH, 78);
		this.frame.image = Game.Seq.core.assets['images/loa/prepare_stock.png'];
		this.frame.frame = 0;
		this.frame.x = -10;
		this.frame.y = -10;
		this.addChild(this.frame);

		// Labels initialized
		this.label1 = new Label();
		this.label2 = new Label();
		this.label3 = new Label();
		this.label4 = new Label();
		this.label5 = new Label();
		this.label6 = new Label();

		// Element initialized
		this.element1 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element2 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element3 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element4 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element5 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element6 = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);

		// Element Setting
		var unit = new Unit("Knight");
		this.element1.setUnit(unit);
		unit.changeJob("Guardian");
		this.element2.setUnit(unit);
		unit.changeJob("Wizard");
		this.element3.setUnit(unit);
		unit.changeJob("Terminator");
		this.element4.setUnit(unit);
		unit.changeJob("Bridge");
		this.element5.setUnit(unit);
		unit.changeJob("Berserker");
		this.element6.setUnit(unit);

		this.addChild(this.element1);
		this.addChild(this.element2);

		for (var i=1; i<=6; i++) {
			// label
			eval('this.label' + i).x = (i-1)%3 * 100 + 50;
			eval('this.label' + i).y = parseInt((i-1)/3) * 35 + 10;
			eval('this.label' + i).color = "white";
			this.addChild(eval('this.label' + i));
			// element
			eval('this.element' + i).x = (i-1)%3 * 95 + 20;
			eval('this.element' + i).y = parseInt((i-1)/3) * 30 + 0;
			eval('this.element' + i).unsetUpdate();
			this.addChild(eval('this.element' + i));
		}

		// HTML5 Canvas border
		var clickmap = new Sprite(P_STOCK_WIDTH, P_STOCK_HEIGHT);
		//  var surface  = new Surface(P_STOCK_WIDTH, P_STOCK_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth = 2;
		//  surface.context.lineJoin = "round";
		//  surface.context.strokeRect(0, 0, P_STOCK_WIDTH, P_STOCK_HEIGHT);
		//  clickmap.image = surface;
		this.addChild(clickmap);

		// Draw labels
		this.update();

		// Event Settings
		this.addEventListener(enchant.Event.TOUCH_START, function(e){
			Game.Seq.prepareController.touchStock(e);
		});
	}

	, update: function() {
		this.label1.text = ' ×  '	+ Game.Seq.prepare.stock.countUnit("Knight");
		this.label2.text = ' ×  ' + Game.Seq.prepare.stock.countUnit("Guardian");
		this.label3.text = ' ×  '	+ Game.Seq.prepare.stock.countUnit("Wizard");
		this.label4.text = ' ×  '	+ Game.Seq.prepare.stock.countUnit("Terminator");
		this.label5.text = ' ×  '		+ Game.Seq.prepare.stock.countUnit("Bridge");
		this.label6.text = ' ×  '	+ Game.Seq.prepare.stock.countUnit("Berserker");
	}
	, selectJob: function(ex, ey) {
		var tx = Math.floor(ex - this.x);
		var ty = Math.floor(ey - this.y);
		var j = Math.floor(tx / (P_STOCK_WIDTH/3));  //3 column horizontal
		var i = Math.floor(ty / (P_STOCK_HEIGHT/2)); //2 column vertical

		var type = "";
		if (i==0 && j==0) {
			type = "Knight";
		} else if (i==0 && j==1) {
			type = "Guardian";
		} else if (i==0 && j==2) {
			type = "Wizard";
		} else if (i==1 && j==0) {
			type = "Terminator";
		} else if (i==1 && j==1) {
			type = "Bridge";
		} else if (i==1 && j==2) {
			type = "Berserker";
		}
		if (type) {
			return type;
		} 
		return false;
	}
	, show: function() {
		this.frame.visible = true;
		for (var i=1; i<7; i++) {
			eval('this.label' + i).visible = true;
			eval('this.element' + i).visible = true;
		}
	}
	, hide: function() {
		this.frame.visible = false;
		for (var i=1; i<7; i++) {
			eval('this.label' + i).visible = false;
			eval('this.element' + i).visible = false;
		}
	}
});


/**
 * Enemy Table
 */
var EnemyTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// Enemy Frame
		this.frame = new Sprite(GAME_WIDTH, 83);
		this.frame.image = Game.Seq.core.assets['images/loa/prepare_enemy.png'];
		this.frame.frame = 0;
		this.frame.x = -10;
		this.frame.y = -10;
		this.addChild(this.frame);

		// Units
		this.units = new Group();
		this.units.x = 5;
		this.units.y = -5;
		this.units.originX = 0;
		this.units.originY = 0;
		this.units.scaleX = 0.44;
		this.units.scaleY = 0.44;
		this.addChild(this.units);

		// Status
		this.name = new Label();
		this.name.color = "white";
		this.name.font  = "10px serif";
		this.name.x = 160;
		this.name.y = 15;
		this.addChild(this.name);

		this.hp = new Label();
		this.hp.color = "white";
		this.hp.font  = "10px serif";
		this.hp.x = 160;
		this.hp.y = 28;
		this.addChild(this.hp);

		this.element = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.unit = new Unit("Knight");
		this.element.setUnit(this.unit);
		this.element.unsetUpdate();
		this.element.x = 265;
		this.element.y = 20;
		this.addChild(this.element);

		this.hp_gauge = new SpriteGauge(B_HPGAUGE_WIDTH, B_HPGAUGE_HEIGHT);
		this.hp_gauge.x = 160;
		this.hp_gauge.y = 43;
		this.addChild(this.hp_gauge);

	}
	, update: function() {
		// Enemy Status
		var enemy   = Game.Seq.battle.enemy;
		this.name.text = enemy.getBattlerName();
		this.hp.text = 'HP ' + enemy.getHp() + '/' + enemy.getMaxHp(); 
		this.hp_gauge.setScale(enemy.getHp(), enemy.getMaxHp());
		this.hp_gauge.draw();
		// Enemy Type
		var type_enemy = enemy.getJobType();
		this.unit.changeJob(type_enemy);
		this.element.clearSurface();
		this.element.setUnit(this.unit);
	}
	, show: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = true;
		}
		for (var i=0; i<this.units.childNodes.length; i++) {
			var sprite = this.units.childNodes[i];
			sprite.visible = true;
		}
	}
	, hide: function() {
		for (var i=0; i<this.childNodes.length; i++) {
			var child = this.childNodes[i];
			child.visible = false;
		}
		for (var i=0; i<this.units.childNodes.length; i++) {
			var sprite = this.units.childNodes[i];
			sprite.visible = false;
		}
	}
	, clear: function() {
		for (var i=0; i<this.units.childNodes.length; i++) {
			var sprite = this.units.childNodes[i];
			sprite.parentNode.removeChild(sprite);
		}
	}
});


/**
 * Formation Table
 */
var FormationTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		var clickmap = new Sprite(P_FORMATION_WIDTH, P_FORMATION_HEIGHT);
		//  var surface	 = new Surface(P_FORMATION_WIDTH, P_FORMATION_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth	= 2;
		//  surface.context.lineJoin	= "round";

		//  // Formation Blocks
		//  var tx, ty = 0;
		//  for (var i=0; i<FORMATION_COLS; i++) {
		//  	for (var j=0; j<FORMATION_ROWS; j++) {
		//  		tx = j * P_BLOCK_WIDTH;
		//  		ty = i * P_BLOCK_HEIGHT;
		//  		surface.context.strokeRect(tx, ty, P_BLOCK_WIDTH, P_BLOCK_HEIGHT);
		//  	}
		//  }
		//  clickmap.image  = surface;	
		this.addChild(clickmap);

		// Selected Block 
		this.rowIndex  = 0;
		this.lineIndex = 0;
		this.selectedBlock = new SelectedBlock(this.rowIndex, this.lineIndex);
		this.addChild(this.selectedBlock);

		// SynchroLines
		this.synchroLines = new SynchroLines(P_FORMATION_WIDTH, P_FORMATION_HEIGHT);
		this.addChild(this.synchroLines);

		// Light
		this.light	 = new SpriteBase(UNIT_WIDTH, UNIT_HEIGHT);
		this.light.surface.context.fillStyle   = "rgba(255,255,255,0.8)";
		this.light.surface.context.arc(UNIT_WIDTH/2, UNIT_WIDTH/2, UNIT_WIDTH/2, 0, Math.PI*2, false);
		this.light.surface.context.fill();
		this.light.x = 0;
		this.light.y = 0;
		this.light.unsetUpdate();
		this.addChild(this.light);

		// light count
		this.count = 0;

		// Event Settings
		this.addEventListener(enchant.Event.TOUCH_START, this.setSelectedPos);
		this.addEventListener(enchant.Event.TOUCH_END, this.setSelectedPos);
		this.on('enterframe', this.randomLight);
	}
	, getRowIndex: function(ex) {
		var tx = Math.floor(ex - this.x);
		var rowIndex = Math.floor(tx / P_BLOCK_WIDTH);
		if (0<=rowIndex && rowIndex<FORMATION_ROWS) {
			return rowIndex;
		}
		return false;
	}
	, getLineIndex: function(ey) {
		var ty = Math.floor(ey - this.y);
		var lineIndex = Math.floor(ty / P_BLOCK_HEIGHT);
		if (0<=lineIndex && lineIndex<FORMATION_COLS) {
			return lineIndex;
		}
		return false;
	}
	, setUnitsPos: function() {
		for (var i=0; i<this.scene.sprites.length; i++) {
			this.scene.sprites[i].setPos();
		}
	}
	, setSelectedPos: function(e) {
		this.rowIndex  = this.getRowIndex(e.x);
		this.lineIndex = this.getLineIndex(e.y);
		this.selectedBlock.setPos(this.rowIndex, this.lineIndex);
	}
	// light
	, randomLight: function(e) {
		this.count++;
		if (this.count>30) {
			var rowIndex  = Math.floor(Math.random()*5);
			var lineIndex = Math.floor(Math.random()*3);
			this.light.x = rowIndex * 60;
			this.light.y = lineIndex * 60;
			this.count = 0;
		}
	}
});

/**
 * in Formation Table, Selected Block
 */
var SelectedBlock = Class.create(Sprite, {
	initialize: function(rowIndex, lineIndex) {
		Sprite.call(this, P_BLOCK_WIDTH, P_BLOCK_HEIGHT);
		this.setPos(rowIndex, lineIndex);

		// HTML5 Canvas border
		var surface = new Surface(P_BLOCK_WIDTH, P_BLOCK_HEIGHT);
		surface.context.lineWidth = 3;
		surface.context.lineJoin = "round"; //"mitter"
		surface.context.strokeStyle = "red";
		surface.context.strokeRect(0, 0, P_BLOCK_WIDTH, P_BLOCK_HEIGHT);
		this.image   = surface;
	}
	, setPos: function(rowIndex, lineIndex){
		this.x = rowIndex * P_BLOCK_WIDTH;
		this.y = lineIndex * P_BLOCK_HEIGHT;
	}
	, show: function() {
		this.visible = true;
	}
	, hide: function() {
		this.visible = false;
	}
});

/**
 * SynchroLines
 */
var SynchroLines = Class.create(Group, {
	initialize: function() {
		Group.call(this, P_FORMATION_WIDTH, P_FORMATION_HEIGHT);

		// Init Settings
		this.sprites = new Array();
	}
	, update: function() {
		this.clear();
		this.draw();
	}
	, draw: function() {
		var synchroLines = Game.Seq.prepare.formation.getSynchroLines();
		for (var i=0; i<synchroLines.length; i++) {
			// SynchroLine 
			var line = synchroLines[i];
			var start_pos = line.getStartPos();
			var end_pos   = line.getEndPos();
			var direction = line.getDirection();
			var type	  = line.getType();
			// Sprite
			var sprite = new Sprite(S_LINE_WIDTH, S_LINE_HEIGHT);
			// Sprite Size
			if (direction == "line") {
				var width  = (end_pos[0] - start_pos[0]) * P_BLOCK_WIDTH;
				sprite.scaleX = width / S_LINE_WIDTH;
			}
			else if (direction == "row") {
				var height = (end_pos[1] - start_pos[1]) * P_BLOCK_HEIGHT;
				sprite.scaleX = height / S_LINE_WIDTH;
				sprite.rotation = 90;
			}
			// Sprite Image
			if (type == "K") {
				sprite.image = Game.Seq.core.assets['images/loa/line01.png'];
			}
			else if (type == "G") {
				sprite.image = Game.Seq.core.assets['images/loa/line02.png'];
			}
			else if (type == "W") {
				sprite.image = Game.Seq.core.assets['images/loa/line03.png'];
			}
			// Sprite Position
			if (direction == "line") {
				sprite.x = start_pos[0] * P_BLOCK_WIDTH + (UNIT_WIDTH / 2);
				sprite.y = start_pos[1] * P_BLOCK_HEIGHT + (UNIT_HEIGHT / 2);
			}
			else if (direction == "row") {
				sprite.x = start_pos[0] * P_BLOCK_WIDTH + (UNIT_WIDTH / 2) + 10;
				sprite.y = start_pos[1] * P_BLOCK_HEIGHT + (UNIT_HEIGHT / 2) + 10;
			}
			// Sprite Central Position
			sprite.originX = 0;
			sprite.originY = 0;

			// Add to Scene
			this.addChild(sprite);
			this.sprites.push(sprite);
		}
	}
	, clear: function() {
		for (var i=0; i<this.sprites.length; i++) {
			var sprite = this.sprites[i];
			sprite.parentNode.removeChild(sprite);
		}
		this.sprites = new Array();
	}
});


/**
 * Display Table
 */
var DisplayTable = Class.create(Group, {
	initialize: function() {
		Group.call(this);

		// HTML5 Canvas border
		var clickmap = new Sprite(P_DISPLAY_WIDTH, P_DISPLAY_HEIGHT);
		//  var surface	 = new Surface(P_DISPLAY_WIDTH, P_DISPLAY_HEIGHT);
		//  surface.context.strokeStyle = "black";
		//  surface.context.lineWidth	= 2;
		//  surface.context.lineJoin	= "round";
		//  surface.context.strokeRect(0, 0, P_DISPLAY_WIDTH, P_DISPLAY_HEIGHT);
		//  clickmap.image  = surface;	
		this.addChild(clickmap);

		// Labels initialized
		this.label1_1 = new Label();
		this.label1_2 = new Label();
		this.label1_3 = new Label();
		this.label2 = new Label();
		this.label3 = new Label();

		this.label1_1.color = "white";
		this.label1_2.color = "white";
		this.label1_3.color = "white";
		this.label2.color = "white";
		this.label3.color = "white";

		this.label1_1.x = 10;
		this.label1_2.x = 10;
		this.label1_3.x = 10;
		this.label1_1.y = 5;
		this.label1_2.y = 17;
		this.label1_3.y = 30;
		this.addChild(this.label1_1);
		this.addChild(this.label1_2);
		this.addChild(this.label1_3);

		this.label2.x = 77;
		this.label2.y = 17;
		this.addChild(this.label2);

		this.label3.x = 155;
		this.label3.y = 5;
		this.label3.font = "10px serif";
		this.label3.text = "Type";
		this.addChild(this.label3);

		// Element Setting
		this.element = new SpriteUnit(UNIT_WIDTH, UNIT_HEIGHT);
		this.element.x = 175;
		this.element.y = 10;
		this.unit = new Unit();
		this.element.setUnit(this.unit);
		this.element.unsetUpdate();
		this.addChild(this.element);
		// 本当はBlinkさせて強調させたいがJobの変更と合致しない
		//  this.element.blinkOn();

		// Button Setting
		this.button = new Sprite(80, 46);
		this.button.image = Game.Seq.core.assets['images/loa/battle_start_01_mini.png'];
		this.button.x = 222;
		this.button.y = 2;
		this.addChild(this.button);
		// 戦闘開始状態を満たした場合のみ可視化する
		this.button.visible = false;

		// Event Settings
		this.button.addEventListener(enchant.Event.TOUCH_START, function(e){
			// 戦闘開始をタッチ
			Game.Seq.prepareController.touchStartBattle();
		});
	}
	, update: function() {
		var formation = this.scene.model.formation;
		// Formation parameter
		this.label1_1.text = '火 '	+ Math.floor(formation.getStr());
		this.label1_2.text = '水 '	+ Math.floor(formation.getDef());
		this.label1_3.text = '雷 '	+ Math.floor(formation.getMgc());

		// バトラーのHPが指定されていれば描画する
		var hp = this.scene.model.getBattlerHp();
		if (hp) {
			this.label2.text = 'HP' + hp + '/' + formation.getMaxHp(); 
		} else {
			this.label2.text = 'HP' + formation.getMaxHp() + '/' + formation.getMaxHp(); 
		}

		// Formation type
		if ("str" == formation.getType()) {
			this.unit.changeJob("Knight");
			this.element.clearSurface();
			this.element.setUnit(this.unit);
		} else if ("def" == formation.getType()) {
			this.unit.changeJob("Guardian");
			this.element.clearSurface();
			this.element.setUnit(this.unit);
		} else if ("mgc" == formation.getType()) {
			this.unit.changeJob("Wizard");
			this.element.clearSurface();
			this.element.setUnit(this.unit);
		} else {
			// TypeのElementに何も表示させない
			this.unit.changeJob("");
			this.element.clearSurface();
			this.element.setUnit(this.unit);
		}

		// Formationに少なくとも一体ユニットが配置されていれば戦闘開始
		if (this.scene.model.isPrepareComplete()) {
			//this.label4.text = '戦闘開始';
			this.button.visible = true;
		} else {
			//this.label4.text = '戦闘準備';
			this.button.visible = false;
		}
	}
});


