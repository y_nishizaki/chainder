/**
 * debugScene.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var DebugScene = new function() {
	var test;
	var title;
	var command1;

	var self = function DebugScene() {
		Scene.apply(this, arguments);

		// Common Settings
		this.backgroundColor = 'white';
		if (typeof model == 'undefined') model = "";
		this.model = model;
		this.test	 = new Test();
		this.sprites = new Array();

		var title = new Label();
		title.text = "Debug Room";
		title.x = (GAME_WIDTH / 2) - (title._boundWidth / 2);
		title.y = 30;

		var frame = new Sprite(320, 320);
		var surface = new Surface(320, 320);
		surface.context.lineWidth = 2;
		surface.context.strokeStyle = "black";
		surface.context.strokeRect(0, 0, 320, 320);
		surface.context.fillStyle = "rgba(200,200,200,0.8)";
		surface.context.fillRect(0, 0, 320, 320);
		frame.image = surface;

		this.addChild(frame);
		this.addChild(title);

		/**
		 * Sprite Base Test
		 */
		var command1 = new Label();
		command1.text = "Sprite Base";
		command1.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1.y	= 150;

		var command1_1 = new Label();
		command1_1.text = "mirror";
		command1_1.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_1.y	= 170;

		var command1_2 = new Label();
		command1_2.text = "blink";
		command1_2.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_2.y	= 190;

		var command1_3 = new Label();
		command1_3.text = "job";
		command1_3.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_3.y	= 210;

		var command1_4 = new Label();
		command1_4.text = "collapse";
		command1_4.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_4.y	= 230;

		var command1_5 = new Label();
		command1_5.text = "damage";
		command1_5.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_5.y	= 250;

		var command1_6 = new Label();
		command1_6.text = "drag";
		command1_6.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_6.y	= 270;

		var command1_7 = new Label();
		command1_7.text = "flush";
		command1_7.x	= (GAME_WIDTH / 2) - (command1._boundWidth /2);
		command1_7.y	= 290;

		var knight		= new Unit("Knight");
		var sprite_base = new SpriteBase(16, 16);
		sprite_base.setUnit();

		this.addChild(command1);
		this.addChild(command1_1);
		this.addChild(command1_2);
		this.addChild(command1_3);
		this.addChild(command1_4);
		this.addChild(command1_5);
		this.addChild(command1_6);
		this.addChild(command1_7);
		this.addChild(sprite_base);

		// Event Settings
		command1.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.spriteBaseTest();
		});
		command1_1.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.turnMirror();
		});
		command1_2.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.turnBlink();
		});
		command1_3.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.turnJob();
		});
		command1_4.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.effectCollapse();
		});
		command1_5.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.effectDamage();
		});
		command1_6.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.turnDrag();
		});
		command1_7.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.turnFlush();
		});


		/**
		 * Sprite Effect Test
		 */
		var command2 = new Label();
		command2.text = "Damage Effect";
		command2.x	= (GAME_WIDTH / 2) + (GAME_WIDTH / 4) -20;
		command2.y	= 150;

		var command2_1 = new Label();
		command2_1.text = "Fire Effect";
		command2_1.x	= (GAME_WIDTH / 2) + (GAME_WIDTH / 4) -20;
		command2_1.y	= 170;

		var command2_2 = new Label();
		command2_2.text = "Blizzard Effect";
		command2_2.x	= (GAME_WIDTH / 2) + (GAME_WIDTH / 4) -20;
		command2_2.y	= 190;

		var command2_3 = new Label();
		command2_3.text = "Thunder Effect";
		command2_3.x	= (GAME_WIDTH / 2) + (GAME_WIDTH / 4) -20;
		command2_3.y	= 210;

		var command2_4 = new Label();
		command2_4.text = "Light Effect";
		command2_4.x	= (GAME_WIDTH / 2) + (GAME_WIDTH / 4) -20;
		command2_4.y	= 230;

		this.addChild(command2);
		this.addChild(command2_1);
		this.addChild(command2_2);
		this.addChild(command2_3);
		this.addChild(command2_4);

		command2.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.damageTest(e);
		});
		command2_1.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.fireTest(e);
		});
		command2_2.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.blizzardTest(e);
		});
		command2_3.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.thunderTest(e);
		});
		command2_4.addEventListener(enchant.Event.TOUCH_START, function(e) {
			Game.Seq.debugController.lightTest(e);
		});
	}

	var uber = enchant.Scene.prototype;

	self.prototype = extend(uber, {
		constructor: self
	});

	return self;
}

