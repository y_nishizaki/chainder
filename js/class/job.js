/**
 * job.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Job = new function() {
	var type;
	var alias;
	var abbr;
	var maxhp, str, def, mgc;
	var highest, lowest;
	var arroundWeaker;
	var image_src, image_frame;

	var self = function Job(job_name) {
		// Randomがjob_nameに指定されている場合は確率で選ぶ
		if (job_name == 'R'|| job_name == 'Random') {
			job_name = this.getRandomJobName();
		}
		// ユニットの基礎パラメータの生成
		switch (job_name) {
			case "Knight":
			case "K":
				this.type	= "Knight";
				this.alias	= "火";
				this.abbr	= "K";
				this.maxhp	= 10;
				this.str	= 3;
				this.def	= 2;
				this.mgc	= 1;
				this.highest	= "str";
				this.lowest		= "mgc";
				this.arroundWeaker	= false;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 0;
				break;
			case "Guardian":
			case "G":
				this.type	= "Guardian";
				this.alias	= "水";
				this.abbr	= "G";
				this.maxhp	= 10;
				this.str	= 1;
				this.def	= 3;
				this.mgc	= 2;
				this.highest	= "def";
				this.lowest		= "str";
				this.arroundWeaker	= false;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 1;
				break;
			case "Wizard":
			case "W":
				this.type	= "Wizard";
				this.alias	= "雷";
				this.abbr	= "W";
				this.maxhp	= 10;
				this.str	= 2;
				this.def	= 1;
				this.mgc	= 3;
				this.highest	= "mgc";
				this.lowest		= "def";
				this.arroundWeaker	= false;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 2;
				break;
			case "Bridge":
			case "Br":
				this.type	= "Bridge";
				this.alias	= "光";
				this.abbr	= "Br";
				this.maxhp	= 10;
				this.str	= 1;
				this.def	= 1;
				this.mgc	= 1;
				this.highest	= false;
				this.lowest		= false;
				this.arroundWeaker	= false;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 4;
				break;
			case "Terminator":
			case "Tr":
				this.type	= "Terminator";
				this.alias	= "風";
				this.abbr	= "Tr";
				this.maxhp	= 10;
				this.str	= 1;
				this.def	= 1;
				this.mgc	= 1;
				this.highest	= false;
				this.lowest		= false;
				this.arroundWeaker	= false;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 3;
				break;
			case "Berserker":
			case "Bs":
				this.type	= "Berserker";
				this.alias	= "闇";
				this.abbr	= "Bs";
				this.maxhp	= 10;
				this.str	= 12;
				this.def	= 12;
				this.mgc	= 12;
				this.highest	= false;
				this.lowest		= false;
				this.arroundWeaker	= true;
				this.image_src		= 'images/loa/element.png';
				this.image_frame	= 5;
				break;
			default :
				this.type	= "Nobody";
				this.alias	= "ナナシ";
				this.abbr	= "N";
				this.maxhp	= 1;
				this.str	= 1;
				this.def	= 1;
				this.mgc	= 1;
				this.highest	= false;
				this.lowest		= false;
				this.arroundWeaker	= false;
				this.image_src		= false;//'images/icon1.png';
				this.image_frame	= 1;
				break;
		}
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getType: function() {
			return this.type;
		}
		, getAlias: function() {
			return this.alias;
		}
		, getAbbr: function() {
			return this.abbr;
		}
		, getMaxHp: function() {
			return this.maxhp;
		}
		, getStr: function() {
			return this.str;
		}
		, getDef: function() {
			return this.def;
		}
		, getMgc: function() {
			return this.mgc;
		}
		, getHighest: function() {
			return this.highest;
		}
		, getLowest: function() {
			return this.lowest;
		}
		, getImageSrc: function() {
			return this.image_src;
		}
		, getImageFrame: function() {
			return this.image_frame;
		}
		, getRandomJobName: function() {
			var rand = Math.floor(Math.random()*100);
			var job_name = "";
			// 基本ユニット65％、Tr15％、Br15％、Bs5％
			if (rand < 65) {
				var rand2 = Math.floor(Math.random()*3);
				switch (rand2) {
					case 0:
						job_name = 'K';
						break;
					case 1:
						job_name = 'G';
						break;
					case 2:
						job_name = 'W';
						break;
					default:
						job_name = '';
						break;
				}
			} else if (rand < 80) {
				job_name = 'Tr';
			} else if (rand < 95) {
				job_name = 'Br';
			} else {
				job_name = 'Bs';
			}
			return job_name;
		}

		/**
		 * boolean
		 */
		, isArroundWeaker: function() {
			return this.arroundWeaker;
		}
	};

	return self;
}

