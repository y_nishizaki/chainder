/**
 * battleController.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var BattleController = new function() {

	var self = function BattleController() {
		Controller.apply(this, arguments);
	}

	var uber = Controller.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * Battle Reset 
		 */
		, reset: function() {
			this.model.reset();
			this.unsetAllSprites();
		}

		/**
		 * Battle Start 
		 */
		, start: function() {
			if (this.model.isBeforeBattle()) {
				// battle の状態を戦闘中に変更
				this.model.setBattleState("start");
				// battle の初期化処理
				this.battleInit();
			} else {
				// Tactics Phase 陣形変更の終了
				this.model.changeTacticsProgress("finish");
			}
		}

		/**
		 * Battle Initialize Process
		 */
		, battleInit: function() {
			// Create Sprite 
			this.setFriendsSprites();
			this.setEnemySprites();
			// HP MAX
			this.allRecovery();
			// Display Status
			this.drawDisplay();
			// Remove before Battle Result
			this.scene.removeCutIn();
			// 戦闘回数をカウントアップ
			this.model.countUp();
		}

		/**
		 * Battle Enterframe Process
		 */
		, enterFrameFunc: function() {
			// バトル中かの判定
			if (this.model.isInBattle()) {
				// loop AttackPhase and TacticsPhase
				var phase = this.model.getPhase();
				switch (phase) {
					case 0:		// Attack Phase 
						this.attackPhaseProcess();
						break;
					case 1:		// Tactics Phase
						this.tacticsPhaseProcess();
						break;
					default :
						break;
				}
			}
		}

		/**
		 * in Enterframe, Case of Attack Phase
		 */
		, attackPhaseProcess: function() {
			var progress = this.model.getAttackProgress();
			switch (progress) {
				case 0:		// Init
					// 攻撃の処理
					this.model.attackPhase();
					// タイプを描画
					this.drawType();
					// ダメージ表示のアニメーション待ち
					this.model.changeAttackProgress("waiting");
					break;
				case 1:		// Waiting
					// アニメーションの進行と処理
					this.attackWaitProcess();
					break;
				case 2:		// Finish
					// Tactics Phaseへ
					this.model.changePhase("tactics");
					break;
				default :
					break;
			}
		}
		, attackWaitProcess: function() {
			// 攻撃アニメーション待ちの状態を取得
			var atk_wait_process = this.model.getAttackWaitProcess();
			switch (atk_wait_process) {
				case 0:
					// 攻撃エフェクトを描画
					this.scene.addAttackEffect();
					break;
				case 1:
					// 攻撃エフェクト待ち
					break;
				case 2:
					// 攻撃後のステータスを描画
					this.drawDisplay();
					// 攻撃のDamageを描画
					this.drawDamage("friends");
					this.drawDamage("enemy");
					// HPを描画
					this.drawHp();
					break;
				case 3:
					// ダメージエフェクト待ち
					break;
				case 4:
					// エフェクトの描画待ち完了
					if (!this.model.getResult()) {
						this.showTransferTactics();
					}
					// 終了判定
					if (this.model.getResult()) {
						this.finish();
					} 
					break;
				case 5:
					// Tactics Phaseへ クリック待ち
				default:
					break;
			}
			// 待ちフレームカウントを進める
			this.model.countAttackWait();
		}
		, showTransferTactics: function() {
			// 攻撃後の入力待ち
			this.scene.appearTransferTactics();
			this.scene.cutin.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.battleController.touchTransferTactics();
			});
		}
		, touchTransferTactics: function() {
			// カットインの破棄
			this.scene.removeCutIn();
			// Attack Phase終了。Tactics Phaseへ
			this.model.changeAttackProgress("finish");
		}

		/**
		 * in Enterframe, Case of Tactics Phase
		 */
		, tacticsPhaseProcess: function() {
			var progress = this.model.getTacticsProgress();
			switch (progress) {
				case 0:		// Init
					// フェーズ進行を、プレイヤーの陣形変更待ちに変更
					this.model.changeTacticsProgress("waiting");
					// prepare scene 初期化
					Game.Seq.prepareScene.initialize(Game.Seq.prepare);
					// prepare sceneに変更
					Game.Seq.changeScene(Game.Seq.prepareScene);
					// prepare に味方バトラーを渡す
					Game.Seq.prepare.tacticsInit(this.model.friends);
					// prepare scene描画
					Game.Seq.prepareController.setFormationSprites();
					Game.Seq.prepareController.draw();
					break;
				case 1:		// Waiting
					// prepare scene で陣形変更を行う
					// battle scene に戻ってきた処理は this.start() 参照
					break;
				case 2:		// Finish
					// 敵の陣形変更の処理
					this.model.enemy.shuffleFormation();
					// 陣形変更の結果を描画
					this.clearAllUnits();
					this.setFriendsSprites();
					this.setEnemySprites();
					// Attack Phaseへ
					this.model.changePhase("attack");
					break;
				default :
					break;
			}
		}

		/**
		 * Battle Finish Process
		 */
		, finish: function() {
			var result = this.model.getResult();
			switch (result) {
				case "WIN":
					this.model.win();
					this.scene.appearWin();
					// Lobby.current_challengerをKnockDown
					Game.Seq.lobbyController.knockDownCurrentChallenger();
					// Enemyを赤く点滅させて消す
					var sprites = this.scene.sprites;
					for (var i=0; i<sprites.length; i++) {
						var sprite = sprites[i];
						if (sprite.battler_type == "enemy") {
							sprite.collapse();
						}
					}
					break;
				case "LOSE":
					this.model.lose();
					this.scene.appearLose();
					break;
				case "DRAW":
					this.model.drawGame();
					this.scene.appearDrawGame();
					break;
				default:
					break;
			}
			return false;
		}

		/**
		 * Touch Cutin
		 */
		, touchCutIn: function() {
			var result = this.model.getResult();
			switch (result) {
				case "WIN":
				case "DRAW":
					// Game Clear判定
					Game.Seq.lobbyController.judgeGameClear();
					// LobbySceneへ
					Game.Seq.changeScene(Game.Seq.lobbyScene);
					break;
				case "LOSE":
					// GameOverの場合StartingSceneへ
					//  Game.Seq.changeScene(Game.Seq.startingScene);
					// LobbySceneへ
					Game.Seq.changeScene(Game.Seq.lobbyScene);
					break;
			}
		}

		/**
		 * battle sprite
		 */
		, setFriendsSprites: function() {
			// Friend
			var f_units = this.model.friends.formation.getUnits();
			for (var i=0; i<f_units.length; i++) {
				var unit = f_units[i];
				var sprite  = new SpriteBattler(UNIT_WIDTH, UNIT_HEIGHT);
				sprite.setUnit(unit);
				sprite.setPos();
				this.scene.sprite	= sprite;
				this.scene.sprites.push(sprite);
				this.scene.friendsTable.addChild(sprite);
				this.scene.sprite.battler_type	= "friends";
			}
		}
		, setEnemySprites: function() {
			// Enemy
			var e_units = this.model.enemy.formation.getUnits();
			for (var i=0; i<e_units.length; i++) {
				var unit = e_units[i];
				var sprite  = new SpriteBattler(UNIT_WIDTH, UNIT_HEIGHT);
				sprite.setUnit(unit);
				sprite.setPos();
				this.scene.sprite	= sprite;
				this.scene.sprites.push(sprite);
				this.scene.enemyTable.addChild(sprite);
				this.scene.sprite.battler_type	= "enemy";
			}
		}
		, clearAllUnits: function() {
			for (var i=0; i<this.scene.sprites.length; i++) {
				var sprite = this.scene.sprites[i];
				sprite.parentNode.removeChild(sprite);
			}
			// スプライトの参照を初期化
			this.scene.sprites = new Array();
		}

		/**
		 * All Battler's HP rcovery Max
		 */
		, allRecovery: function() {
			this.model.friends.completeRecovery();
			this.model.enemy.completeRecovery();
		}

		/**
		 * Status On battleScene.battleDisplay
		 */
		, drawDisplay: function() {
			this.scene.battleDisplay.update();
		}
		, drawType: function() {
			this.scene.battleDisplay.drawType();
		}
		, drawHp: function() {
			this.scene.battleDisplay.drawHp();
		}

		/**
		 * Damage Effect on battleScene.battleDisplay
		 */
		, drawDamage: function(type) {
			var label_hp;
			var value = 0;
			var damage_type = 0;
			if (type == "friends") {
				label_hp = this.scene.battleDisplay.label1_2;
				value    = this.model._last_f_damage;
				damage_type = this.model._last_f_damage_type;
			} else if (type =="enemy") {
				label_hp = this.scene.battleDisplay.label2_2;
				value    = this.model._last_e_damage;
				damage_type = this.model._last_e_damage_type;
			}
			// SpriteDamage
			var sprite	= new SpriteDamage(value, damage_type);
			sprite.x = label_hp.x + 30;
			sprite.y = label_hp.y - 10;
			this.scene.addChild(sprite);

		}
	});

	return self;
}


