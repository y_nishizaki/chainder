/**
 * Controller.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Controller = new function() {
	var model;
	var scene;

	var self = function Controller(model, scene) {
		if (typeof model === 'undefined')	model = "";
		if (typeof scene === 'undefined')	scene = "";
		this.model = model;
		this.scene = scene;
		this.sprites = new Array();
	}

	self.prototype = {
		constructor: self
		, unsetAllSprites: function() {
			// $B%9%W%i%$%HGK4~(B
			var sprites = this.scene.sprites;
			for (var i=0; i<sprites.length; i++) {
				var sprite = sprites[i];
				// Sprite$B$r2rJ|$9$k(B
				sprite.dispose();
			}
			// sprites$B$r=i4|2=(B
			this.scene.sprites = new Array();
		}
	};

	return self;
}

