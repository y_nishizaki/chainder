/**
 * startingController.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var StartingController = new function() {
	var model;
	var view;

	var self = function StartingController() {
		Controller.apply(this, arguments);
	}

	var uber = Controller.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * Click label NewGame
		 */
		, NewGame: function() {
			// Game Opening
			this.scene.gameOpeningImage = new Sprite(GAME_WIDTH, GAME_HEIGHT);
			this.scene.gameOpeningImage.image = Game.Seq.core.assets['images/loa/opening.png'];
			this.scene.gameOpeningImage.x = 0;
			this.scene.gameOpeningImage.y = 0;
			this.scene.addChild(this.scene.gameOpeningImage);
			// Next
			this.scene.next_btn = new SpriteBase(69, 37);
			this.scene.next_btn.image = Game.Seq.core.assets['images/loa/btn_next.png'];
			this.scene.next_btn.x = 240;
			this.scene.next_btn.y = 260;
			this.scene.addChild(this.scene.next_btn);
			this.scene.next_btn.flushOn();

			// $BM7$SJ}(B
			this.scene.manual_btn = new SpriteBase(69, 37);
			this.scene.manual_btn.image = Game.Seq.core.assets['images/loa/btn_manual.png'];
			this.scene.manual_btn.x = 30;
			this.scene.manual_btn.y = 260;
			this.scene.addChild(this.scene.manual_btn);

			// Event Settings
			this.scene.next_btn.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.startingController.NewGameStart();
			});
			this.scene.manual_btn.addEventListener(enchant.Event.TOUCH_START, function(e){
				Game.Seq.startingController.NewGameStart("show_manual");
			});
		}
		, NewGameStart: function(manual) {
			if (typeof manual === 'undefined')	manual = false;
			// Game initialize
			Game.Seq.gameInit();
			// Lobby Scene initialize
			Game.Seq.lobbyController.start();
			// Remove Opening Image
			this.scene.removeChild(this.scene.gameOpeningImage);
			this.scene.removeChild(this.scene.next_btn);
			// Change Lobby Scene
			Game.Seq.changeScene(Game.Seq.lobbyScene);
			// Set Tutorial 
			if (manual == "show_manual") {
				Game.Seq.lobby.setShowManual(true);
			}
			Game.Seq.lobbyController.setTutorial();
		}

		/**
		 * Click label Battle
		 */
		, BattleTest: function() {
			// Game initialize
			Game.Seq.gameInit();
			// Battle Initialize
			Game.Seq.battleController.start();
			// Change Battle Scene
			Game.Seq.changeScene(Game.Seq.battleScene);
		}

		/**
		 * Click label Debug
		 */
		, DebugTest: function() {
			Game.Seq.debugInit();
			Game.Seq.changeScene(Game.Seq.debugScene);
		}

		/**
		 * Click label Lobby
		 */
		, LobbyTest: function() {
			// Game initialize
			Game.Seq.gameInit();
			// Lobby Initialize
			Game.Seq.lobbyController.start();
			// Change Lobby Scene
			Game.Seq.changeScene(Game.Seq.lobbyScene);
		}
	});

	return self;
}


