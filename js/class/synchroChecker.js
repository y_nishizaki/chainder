/**
 * synchroChecker.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SynchroChecker = new function() {
	var list;
	var units;
	var synchroLines;				// SynchroLineの配列
	var start_pos, end_pos;
	var search_type;
	var searching;


	var self = function SynchroChecker(list_arg, units_args) {
		if (typeof list_arg === 'undefined')	return false;
			this.list = list_arg;

		if (typeof units_args === 'undefined')	return false;
			this.units = units_args;

		this.synchroLines = new Array();
		this.start_pos = new Array();
		this.end_pos = new Array();
		this.search_type = "";
		this.searching = false;
		this.complete = false;
		this.checkBr = false;
		this.hasBr = false;
		this.before_synchroLines = new Array();
	}

	self.prototype = {
		constructor: self
		/**
		 * reset
		 */
		, reset: function() {
			this.start_pos = new Array();
			this.end_pos = new Array();
			this.search_type = "";
			this.searching = false;
			this.complete = false;
			this.checkBr = false;
			this.hasBr = false;
		}

		/**
		 * getter
		 */
		, getSynchroLines: function() {
			return this.synchroLines;
		}

		/**
		 * boolean
		 */
		, isSearching: function() {
			return this.searching;
		}
		, isComplete: function() {
			return this.complete;
		}

		/**
		 * setter
		 */
		, setList: function(list) {
			this.list = list;
		}
		, setUnits: function(units) {
			this.units = units;
		}

		/**
		 * update
		 */
		, update: function() {
			// 一つ前のバージョンをバックアップ
			this.before_synchroLines = this.synchroLines;
			// シンクロラインの初期化
			this.synchroLines = new Array();
			// シンクロラインの検出
			for (var i=0; i<this.list.length; i++) {
				this.reset();
				this.searchLine(i);	
			}
			for (var j=0; j<this.list[0].length; j++) {
				this.reset();
				this.searchRow(j);	
			}
		}

		/**
		 * Search SynchroLine
		 */
		, searchRow: function(rowIndex, start_index) {
			// 検索対象の配列を取得
			var target = this.getRow(rowIndex);

			// 起点の検出位置の確認
			if (typeof start_index === 'undefined') start_index = 0;
			if (start_index > target.length ) return false;

			// シンクロラインの起点になるタイプを検出する
			for (var i=start_index; i<target.length; i++) {
				var abbr = target[i];
				if (abbr=="K" || abbr=="G" || abbr=="W") {
					// シンクロラインがどこまで続くかの判別に状態を変更
					this.searching = true;
					// シンクロラインの起点を記録
					var x = rowIndex;
					var y = i;
					this.start_pos = new Array(x,y);
					this.search_type = abbr;
					break;
				}
			}
			// 起点が検出できた場合シンクロラインが成立するか調べる
			if (this.isSearching()) {
				// 検索を開始する座標
				var rowIndex  = this.start_pos[0];	// x : (x, y)
				var lineIndex = this.start_pos[1];	// y : (x, y)
				// 始点からひとつずらす
				lineIndex += 1;
				// 残りのマスを評価する
				for (var i=lineIndex; i<target.length; i++) {
					var next = this.searchNext(rowIndex,i);
					// 同じタイプのユニットを検出できたらシンクロライン成立
					if (next == this.search_type) {
						// シンクロライン成立
						this.complete = true;
						// 終点を更新
						this.end_pos = new Array(rowIndex, i);
						// 途中でBrを挟んでいればBrを持つ
						if (this.checkBr) {
							this.hasBr = true;
						}
					}
					// brChainは読み飛ばす
					else if (next == "Br") {
						this.checkBr = true;
					}
					// 同じタイプかBr以外に当たれば、シンクロラインの検出を終わる
					else {
						break;
					}
				}

				// シンクロラインが成立していれば
				if (this.isComplete()) {
					// Trの検出
					var start_index = this.start_pos[1];
					var end_index   = this.end_pos[1];
					// before
					var before = this.searchNext(rowIndex, start_index - 1);
					if (before == "Tr") {
						this.start_pos[1] = start_index - 1;
					}
					// after
					var after = this.searchNext(rowIndex, end_index + 1);
					if (after == "Tr") {
						this.end_pos[1] = end_index + 1;
					}

					// シンクロラインが所持するユニットのリスト
					var units = new Array();
					for (var i=this.start_pos[1]; i<this.end_pos[1]; i++) {
						var unit = this.searchUnit(rowIndex, i);
						units.push(unit);
					}

					// シンクロラインの生成
					var params = {
						'type'      : this.search_type,
						'start_pos' : this.start_pos,
						'end_pos'   : this.end_pos,
						'hasBr'     : this.hasBr,
						'units'     : units,
						'direction' : "row",
					}
					var synchroLine = new SynchroLine(params);
					// シンクロラインのリストに追加
					this.synchroLines.push(synchroLine);

					// 終点を始点にして再度検索を行う
					var next_start_index = this.end_pos[1];
					// 検索で使用したフラグの初期化
					this.reset();
					// Search
					this.searchRow(rowIndex, next_start_index);
				} else {
					// 始点をひとつ進めて再度検索を行う
					var next_start_index = this.start_pos[1] + 1;
					// 検索で使用したフラグの初期化
					this.reset();
					// Search
					this.searchRow(rowIndex, next_start_index);
				}
			}
		}

		, searchLine: function(lineIndex, start_index) {
			// 検索対象の配列を取得
			var target = this.getLine(lineIndex);

			// 起点の検出位置の確認
			if (typeof start_index === 'undefined') start_index = 0;
			if (start_index > target.length ) return false;

			// シンクロラインの起点になるタイプを検出する
			for (var i=start_index; i<target.length; i++) {
				var abbr = target[i];
				if (abbr=="K" || abbr=="G" || abbr=="W") {
					// シンクロラインがどこまで続くかの判別に状態を変更
					this.searching = true;
					// シンクロラインの起点を記録
					var x = i;
					var y = lineIndex;
					this.start_pos = new Array(x,y);
					this.search_type = abbr;
					break;
				}
			}
			// 起点が検出できた場合シンクロラインが成立するか調べる
			if (this.isSearching()) {
				// 検索を開始する座標
				var rowIndex  = this.start_pos[0];	// x : (x, y)
				var lineIndex = this.start_pos[1];	// y : (x, y)
				// 始点からひとつずらす
				rowIndex += 1;
				// 残りのマスを評価する
				for (var i=rowIndex; i<target.length; i++) {
					var next = this.searchNext(i,lineIndex);
					// 同じタイプのユニットを検出できたらシンクロライン成立
					if (next == this.search_type) {
						// シンクロライン成立
						this.complete = true;
						// 終点を更新
						this.end_pos = new Array(i, lineIndex);
						// 途中でBrを挟んでいればBrを持つ
						if (this.checkBr) {
							this.hasBr = true;
						}
					}
					// brChainは読み飛ばす
					else if (next == "Br") {
						this.checkBr = true;
					}
					// 同じタイプかBr以外に当たれば、シンクロラインの検出を終わる
					else {
						break;
					}
				}

				// シンクロラインが成立していれば
				if (this.isComplete()) {
					// Trの検出
					var start_index = this.start_pos[0];
					var end_index   = this.end_pos[0];
					// before
					var before = this.searchNext(start_index-1, lineIndex);
					if (before == "Tr") {
						this.start_pos[0] = start_index - 1;
					}
					// after
					var after = this.searchNext(end_index+1, lineIndex);
					if (after == "Tr") {
						this.end_pos[0] = end_index + 1;
					}

					// シンクロラインが所持するユニットのリスト
					var units = new Array();
					for (var i=this.start_pos[0]; i<this.end_pos[0]; i++) {
						var unit = this.searchUnit(i, lineIndex);
						units.push(unit);
					}

					// シンクロラインの生成
					var params = {
						'type'      : this.search_type,
						'start_pos' : this.start_pos,
						'end_pos'   : this.end_pos,
						'hasBr'     : this.hasBr,
						'units'     : units,
						'direction' : "line",
					}
					var synchroLine = new SynchroLine(params);
					// シンクロラインのリストに追加
					this.synchroLines.push(synchroLine);

					// 終点を始点にして再度検索を行う
					var next_start_index = this.end_pos[0];
					// 検索で使用したフラグの初期化
					this.reset();
					// Search
					this.searchLine(lineIndex, next_start_index);
				} else {
					// 始点をひとつ進めて再度検索を行う
					var next_start_index = this.start_pos[0] + 1;
					// 検索で使用したフラグの初期化
					this.reset();
					// Search
					this.searchLine(lineIndex, next_start_index);
				}
			}
		}

		/**
		 *  array's manipulator method
		 */
		, getLine: function(lineIndex) {
			if (typeof lineIndex === 'undefined') lineIndex = this.lineIndex;
			return this.list[lineIndex];
		}
		, getRow: function(rowIndex) {
			if (typeof rowIndex === 'undefined') rowIndex = this.rowIndex;
			var row_array = new Array();
			for (var i=0; i<this.list.length; i++) {
				row_array.push(this.list[i][rowIndex]);
			}
			return row_array;
		}
		, searchNext: function(rowIndex, lineIndex) {
			if (rowIndex < 0 || FORMATION_ROWS-1<rowIndex) return false;
			if (lineIndex < 0 || FORMATION_COLS-1<lineIndex) return false;
			return this.list[lineIndex][rowIndex];
		}
		, searchUnit: function(rowIndex, lineIndex) {
			for (var i=0; i<this.units.length; i++) {
				if (this.units[i].rowIndex == rowIndex && this.units[i].lineIndex == lineIndex) {
					return this.units[i];
				}
			}
			return false;
		}

	};

	return self;
}



