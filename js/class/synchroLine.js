/**
 * synchroLine.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var SynchroLine = new function() {
	var start_pos, end_pos;
	var type;
	var units;
	var hasBr, hasTr;
	var sustain_count;

	var self = function SynchroLine(params) {
		//  var params = {
		//  	'type'      : this.search_type,
		//  	'start_pos' : this.start_pos,
		//  	'end_pos'   : this.end_pos,
		//  	'hasBr'     : this.hasBr,
		//  	'units'     : units,
		//  	'direction' : direction,
		//  }
		
		// Check Args
		if (typeof params.type === 'undefined') return false;
		if (typeof params.start_pos === 'undefined') return false;
		if (typeof params.end_pos === 'undefined') return false;
		if (typeof params.hasBr === 'undefined') return false;
		if (typeof params.units === 'undefined') return false;
		if (typeof params.direction === 'undefined') return false;

		// Make SynchroLine
		this.type = params.type;
		this.start_pos = params.start_pos;
		this.end_pos = params.end_pos;
		this.hasBr = params.hasBr;
		this.units = params.units;
		this.direction = params.direction;

	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getStartPos: function() {
			return this.start_pos;
		}
		, getEndPos: function() {
			return this.end_pos;
		}
		, getDirection: function() {
			return this.direction;
		}
		, getType: function() {
			return this.type;
		}
		, getBr: function() {
			return this.hasBr;
		}
		, getUnits: function() {
			return this.units;
		}
	};

	return self;
}


