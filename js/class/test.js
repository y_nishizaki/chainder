/**
 * test.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Test = new function() {
	var unit_list;				// arrangement list
	var counterX, counterY;		// chainCounter position
	var own_units;				// unit job

	var self = function Test() {
		// default arrangemet
		this.setUnitList("0001");
		// init counterX, counterY
		this.setCounterPos(0,0);
		// set test case of unitManager.units
		this.own_units = {
			"Knight"	: 6,
			"Guardian"	: 5,
			"Wizard"	: 4,
			"Bridge"	: 3,
			"Terminator": 2,
			"Berserker"	: 1
		}
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getUnitList: function() {
			return this.unit_list
		}
		, getCounterY: function() {
			return this.counterY;
		}
		, getCounterX: function() {
			return this.counterX;
		}
		, getOwnUnits: function() {
			return this.own_units
		}

		/**
		 * setter
		 */
		// Test of Arrangement, Formation
		, setUnitList: function(id) {
			if (typeof id === 'undefined') id = 1;
			// arrangemet.list
		    var list0001 = [
		    	["K",  "K",  "Tr",  "",   "Bs" ],
		    	["Br", "W",  "W",	"",   ""   ],
		    	["K",  "G",  "Br",	"G",  ""   ]
		    ];
		    var list0002 = [
		    	["K",  "K",  "Tr",  "",  "K"  ],
		    	["Br", "W",  "W",	"",  "Br"],
		    	["K",  "G",  "",	"",  "K"  ]
		    ];
		    var list0003 = [
		    	["K",  "Br", "K",  "Br", "K"  ],
		    	["Br", "",   "Br", "",   "Bs" ],
		    	["K",  "",   "K",  "",   ""   ]
		    ];
		    var list0004 = [
		    	["Br", "Br", "Br", "Br", "Br" ],
		    	["Br", "",   "Br", "",   "Bs" ],
		    	["K",  "",   "Br", "",   ""   ]
		    ];
		    var list0005 = [
		    	["K", "K", "Br", "K", "G" ],
		    	["K", "",   "W", "W",   "G" ],
		    	["Tr",  "",   "", "G",   "G"   ]
		    ];
			// K　STR特化
		    var list0006 = [
		    	["K", "K", "Br", "K", "K" ],
		    	["K", "K", "Br", "Br", "K" ],
		    	["Tr",  "W", "W", "W", "Tr" ]
		    ];
			// G　DEF特化
		    var list0007 = [
		    	["G", "G", "Br", "G", "G" ],
		    	["G", "G", "Br", "Br", "G" ],
		    	["Tr",  "K", "K", "K", "Tr" ]
		    ];
			// W　MGC特化
		    var list0008 = [
		    	["W", "W", "Br", "W", "W" ],
		    	["W", "W", "Br", "Br", "W" ],
		    	["Tr",  "G", "G", "G", "Tr" ]
		    ];
			// Br, Trバランス
		    var list0009 = [
		    	["K", "K", "Br", "K", "Tr" ],
		    	["G", "Br", "Br", "G", "W" ],
		    	["Tr", "W", "W", "W", "W" ],
		    ];
			// Bs
		    var list0010 = [
		    	["Bs", "", "Bs", "", "Bs" ],
		    	["", "", "", "", "" ],
		    	["Bs", "", "Bs", "", "Bs" ],
		    ];

			// 0001の形式に変換する
			id = id.toString();
			while (id.length < 4) {
				id = "0" + id;
			}
			// update
			this.unit_list =  eval("list"+id);
		}
		// Test of ChainCounter
		, setCounterPos: function(lineIndex, rowIndex) {
			if (typeof rowIndex === 'undefined') rowIndex = 0;
			if (typeof lineIndex === 'undefined') lineIndex = 0;
			this.counterY = lineIndex;
			this.counterX = rowIndex;
			return new Array(this.counterY, this.counterX);
		}
		// Test of UnitManager
		, setOwnUnits: function(own_units) {
			var default_units = {
					"Knight"        : 6,
					"Guardian"      : 5,
					"Wizard"        : 4,
					"Bridge"        : 3,
					"Terminator"	: 2,
					"Berserker"     : 1
			}
			if (typeof own_units === 'undefined') own_units = default_units;

			this.own_units = own_units;
		}

		/**
		 * Sequence Test
		 */
		, sequenceTest: function(Seq) {
			console.log("Sequenceのテストを開始！");
			// Game中で使用するModel, View, Controllerを作成する
			Seq.gameInit();
			console.log("ゲーム初期化処理を実行しました");
			// 参照渡しのテスト 値の変更
			Seq.battle.core._height = 300;
			console.log("Seq.battle.core._height の値を変更しました");
			console.log("Seq.battle.core:");
			console.log(Seq.battle.core);
			console.log("Seq.core:");
			console.log(Seq.core);
			// 参照渡しのテスト 比較
			console.log("Seq.battle.coreとSeq.coreの比較実験");
			if (Seq.core === Seq.battle.core) {
				console.log("same core");
				console.log("Seq.coreはSeq.battle.coreに正常に参照渡しされました");
			} else {
				console.log("another core");
				console.log("Seq.coreはSeq.battle.coreは異なります");
			}
			console.log("Sequenceのテストを終了します");
		}

		/**
		 * Arrangement Test
		 */
		, arrangementTest: function() {
			// using test case 
			var list = this.getUnitList();
			// arrangement
			var arrangement = new Arrangement(list);
			// test
			console.log("Arrangementのテストを開始!");
			console.log(arrangement);
			console.log("現在の陣形：");
			console.log(arrangement.viewList());
			console.log("陣形のパラメータ：");
			console.log(arrangement.viewStatus());
		}

		/**
		 * ChainCounter Test
		 */
		, chainCounterTest: function() {
			var list = this.getUnitList();
			var counterY = this.getCounterY();
			var counterX = this.getCounterX();
			var chainCounter = new ChainCounter(list, counterY, counterX);
			var lonelyFlag = chainCounter.isLonely();
			var line = chainCounter.getLine();
			var row  = chainCounter.getRow();
			var sameJobChain  = chainCounter.getSameJobChain();
			var brChain  = chainCounter.getBrChain();
			var trChain  = chainCounter.getTrChain();
			console.log("ChainCounterのテストを開始!");
			var arrangement = new Arrangement(list);
			console.log("現在の陣形：");
			console.log(arrangement.viewList());
			console.log("ChainCounterのターゲット：");
			var currentTarget = list[counterY][counterX];
			console.log(currentTarget + "（" + counterY + ", "+ counterX +"）");

			console.log("ターゲットを含む行（ヨコ）：");
			console.log(line);
			console.log("ターゲットを含む列（タテ）：");
			console.log(row);
			console.log("sameJobChainの連結数：" + sameJobChain);
			console.log("brChainの連結数     ：" + brChain);
			console.log("trChainの連結数     ：" + trChain);
		}

		/**
		 * UnitManager Test
		 */
		, unitManagerTest: function() {
			var own_units = this.getOwnUnits();
			var unitManager = new UnitManager(own_units);
			console.log("unitManagerのテストを開始！");
			console.log(unitManager);
			var k_cnt = unitManager.countUnit("Knight");
			var g_cnt = unitManager.countUnit("Guardian");
			var w_cnt = unitManager.countUnit("Wizard");
			var br_cnt = unitManager.countUnit("Bridge");
			var tr_cnt = unitManager.countUnit("Terminator");
			var bs_cnt = unitManager.countUnit("Berserker");
			var units_cnt = unitManager.countUnit();
			console.log("Kの残機：" + k_cnt);
			console.log("Gの残機：" + g_cnt);
			console.log("Wの残機：" + w_cnt);
			console.log("Brの残機：" + br_cnt);
			console.log("Trの残機：" + tr_cnt);
			console.log("Bsの残機：" + bs_cnt);
			console.log("ユニットの残機（合計）：" + units_cnt);
			console.log("unitManager.removeUnitのテスト：");
			unitManager.removeUnit("Berserker");
			console.log("Berserkerを削除しました");
			bs_cnt = unitManager.countUnit("Berserker");
			console.log("Bsの残機：" + bs_cnt);
		}

		/**
		 * Stock Test
		 */
		 , stockTest: function() {
			var own_units = this.getOwnUnits();
			console.log(own_units);
			var stock = new Stock(own_units);
			console.log("Stockのテストを開始！");
		 }

		/**
		 * Formation Test
		 */
		 , formationTest: function() {
			var own_units = this.getOwnUnits();
			console.log(own_units);
			var formation = new Formation(own_units);
			console.log("Formationのテストを開始！");
			console.log(formation);
		 }

		/**
		 * Prepare Test
		 */
		 , prepareTest: function() {
			var prepare = new Prepare();
			console.log("Prepareのテストを開始！");
			console.log(prepare);
			console.log("Stockのテスト:");
			console.log(prepare.stock);
			console.log("Stockが所持しているユニット:");
			console.log(prepare.stock.countUnit() + "体");
			console.log(prepare.stock.getUnits());
			console.log("Formationのテスト:");
			console.log(prepare.formation);
			console.log("Formationが所持しているユニット:");
			console.log(prepare.formation.countUnit() + "体");
			console.log(prepare.formation.getUnits());

			console.log("Prepare.transferUnit()のテスト:");
			console.log("ナイトをstockからformationに渡します");
			prepare.transferUnit("Knight", "stock", "formation");
			console.log(prepare.stock.getUnits());
			console.log(prepare.formation.getUnits());
		 }

		/**
		 * Battler Test
		 */
		, battleTest: function() {
			// Battle init
			var battle = new Battle(Game.Seq.core);
			console.log("Battleのテストを開始！");
			console.log(battle);
			// Test Case
			// (7,6) = 100% win

			// Friends
			this.setUnitList(9);
			var f_list = this.getUnitList();
			battle.friends.setList(f_list);
			console.log("friends", battle.friends);
			// Enemy
			this.setUnitList(10);
			var e_list = this.getUnitList();
			battle.enemy.setList(e_list);
			console.log("enemy", battle.enemy);

			// Complete Recovery
			battle.friends.completeRecovery();
			battle.enemy.completeRecovery();
			// Battler Status
			console.log(" - - - - - - - - - - - - - - - - - -");
			console.log("friends_status", battle.friends.formation.arrangement.viewStatus());
			console.log("enemy_status  ", battle.enemy.formation.arrangement.viewStatus());
			console.log("friends_formation");
			console.log(battle.friends.formation.arrangement.viewList());
			console.log("enemy_formation");
			console.log(battle.enemy.formation.arrangement.viewList());
			// Attack Phase Loop
			var cnt = 0;
			while (battle.getResult() == 0) {
				battle.attackPhase();
				// 終了判定
				if (battle.getResult()) {
					console.log(" - - - - - - - - - - - - - - - - - -");
					console.log("バトル結果：", battle.getResult());
					console.log("ターン数　：", battle.getTurn());
					return;
				}
				battle.tacticsPhase();
				cnt += 1;
				if (cnt>30) return false;
			}
		}

		/**
		 * Challenger Test
		 */
		, challengerTest: function() {
			console.log("Challengerのテストを開始！");
			// Challenger
			var id = 0;
			var challenger = new Challenger(id);
			console.log(challenger);
			// Getter Test
			console.log("getId()", challenger.getId());
			console.log("getName()", challenger.getName());
			console.log("getList()", challenger.getList());
			console.log("getType()", challenger.getType());
			console.log("getJobType()", challenger.getJobType());
			console.log("getLevel()", challenger.getLevel());
			// Boolean Test
			console.log("isKnockDown()", challenger.isKnockDown());
			// setter Test
			challenger.setChallenger(1);
			console.log("setChallenger(1)", challenger);
		}

		/**
		 * Lobby Test
		 */
		, lobbyTest: function() {
			console.log("Lobbyのテストを開始！");
			// Lobby
			var lobby = new Lobby();
			console.log("Lobby", lobby);
			// Getter Test
			console.log("getChallengers()", lobby.getChallengers());
			console.log("getCurrentChallenger()", lobby.getCurrentChallenger());
			console.log("getRandomChallenger()", lobby.getRandomChallenger());
			console.log("getChallengerById(1)", lobby.getChallengerById(1));
			// Boolean Test
			console.log("isGameClear()", lobby.isGameClear());
			// Setter Test
			console.log("setChallengerIds()", lobby.setChallengerIds());
			console.log("setCurrentChallenger()", lobby.setCurrentChallenger());

			// GameClear Test
			console.log("GameClearのテストを開始！");
			console.log("挑戦者をランダムに選び倒していきます。");
			for (var i=0; i<lobby.challengers.length; i++) {
				console.log("-------------------");
				console.log("Round:" + i);
				// challengerをランダムに選ぶ
				var battle_target = lobby.getRandomChallenger();
				console.log("getRandomChallenger()", battle_target);
				console.log("Challenger.getName()", battle_target.getName());
				// challengerを倒す
				battle_target.setKnockDown(true);
				// GameClear判定
				if (lobby.isGameClear()) {
					console.log("Game Clear!");
				} else {
					console.log("挑戦者を倒しました");
				}
			}
		}

		/**
		 * Synchronizer Test
		 */
		, synchroTest: function() {
			console.log("Synchronizerのテストを開始！");
			// Formation
			this.setUnitList(5);
			var test_list = this.getUnitList();
			var formation = new Formation();
			formation.setList(test_list);
			console.log("Formation", formation);
			//
			console.log(formation.arrangement.viewList());

			// LineCounter
			var list  = formation.getList();
			var units = formation.getUnits();
			var direction = "line";
			var index	  = 0;

			// SynchroChecker
			var synchroChecker = new SynchroChecker(list, units);
			console.log("synchroChecker", synchroChecker);
			// update
			synchroChecker.update();
			// getSynchroLines()
			var synchroLines = synchroChecker.getSynchroLines();
			console.log("synchroLines", synchroLines);



		}

	};

	return self;
}


