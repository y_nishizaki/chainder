/**
 * formation.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Formation = new function() {
	var arrangement;
	var maxhp, str, def, mgc;

	var self = function Formation(own_units) {
		UnitManager.apply(this, arguments);
		this.arrangement = new Arrangement();
		this.maxhp = this.arrangement.getMaxHp();
		this.str = this.arrangement.getStr();
		this.def = this.arrangement.getDef();
		this.mgc = this.arrangement.getMgc();
		// SynchroLines
		this.synchroChecker = new SynchroChecker(this.getList(), this.getUnits());
		this.synchroLines   = new Array();
	}

	var uber = UnitManager.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * override
		 */
		, addUnit: function(job_type) {
			// UnitManager.addUnit()
			var unit = uber.addUnit.apply(this, arguments);
			// Add rowIndex, lineIndex
			var new_position = this.searchEmpty();
			if (new_position) {
				unit.rowIndex  = new_position[0];
				unit.lineIndex = new_position[1];
			}
			this.update();
			return unit;
		}

		/**
		 * getter
		 */
		, getMaxHp: function() {
			return this.maxhp;
		}
		, getStr: function() {
			return this.str;
		}
		, getDef: function() {
			return this.def;
		}
		, getMgc: function() {
			return this.mgc;
		}
		, getType: function() {
			var arr = new Array();
			arr['str'] = this.getStr();
			arr['def'] = this.getDef();
			arr['mgc'] = this.getMgc();
			// 連想配列から最大値の添字を取り出す
			var max		 = 0;
			var max_type = "";
			for (var key in arr) {
				if (arr[key] > max) {
					max = arr[key];
					max_type = key;
				}
			}
			this.type = max_type;
			
			return this.type;
		}
		, getUnits: function() {
			return this.units;
		}
		, getList: function() {
			return this.arrangement.getList();
		}
		, getSynchroLines: function() {
			return this.synchroLines;
		}

		/**
		 * setter
		 */
		, setList: function(list) {
			// listの配列からunitsを生成する
			this.arrangement.setList(list);
			this.arrangement.update();
			this.units = this.arrangement.getUnits();
			this.update();
		}

		/**
		 * update
		 */
		, update: function() {
			// arrangementにunitsを渡しパラメータを更新する
			this.arrangement.setUnits(this.units);
			this.arrangement.update();
			this.units = this.arrangement.getUnits();
			// SynchroCheckerにlistとunitsを渡しsynchroLinesを更新する
			this.synchroChecker.setList(this.getList());
			this.synchroChecker.setUnits(this.getUnits());
			this.synchroChecker.update();
			this.synchroLines = this.synchroChecker.getSynchroLines();
			// パラメータの更新
			this.maxhp = this.arrangement.getMaxHp();
			this.str   = this.arrangement.getStr();
			this.def   = this.arrangement.getDef();
			this.mgc   = this.arrangement.getMgc();
			this.type  = this.getType();
		}

		/**
		 * Search Formation's arrangement
		 */
		, searchEmpty: function() {
			return this.arrangement.searchEmpty();
		}
		, searchUnit: function(rowIndex, lineIndex) {
			for (var i=0; i<this.units.length; i++) {
				if (this.units[i].rowIndex == rowIndex && this.units[i].lineIndex == lineIndex) {
					return this.units[i];
				}
			}
			return false;
		}

		/**
		 * Change Formation Arrangement
		 */
		, swapPos: function(start_pos, end_pos) {
			// Require array(rowIndex, lineIndex)
			if (typeof start_pos === 'array') return false;
			if (typeof end_pos === 'array') return false;
			
			// target unit
			var start_pos_unit = this.searchUnit(start_pos[0], start_pos[1]);
			var end_pos_unit   = this.searchUnit(end_pos[0], end_pos[1]);
			// replace
			start_pos_unit.rowIndex  = end_pos[0];
			start_pos_unit.lineIndex = end_pos[1];
			if (end_pos_unit) {
				end_pos_unit.rowIndex  = start_pos[0];	
				end_pos_unit.lineIndex = start_pos[1];	
			}
		}
		, shuffle: function() {
			// ユニット数の取得
			var l = this.units.length;
			var rand = Math.floor(Math.random() * l);
			// 変更の対象になるユニットを選ぶ
			var selectedUnit = this.units[rand];
			// 交換先のPosを選ぶ
			var rowIndex = Math.floor(Math.random() * 4);
			var lineIndex = Math.floor(Math.random() * 2);
			var replaceUnit = this.searchUnit(rowIndex, lineIndex);
			// swap position
			var start_pos = new Array(selectedUnit.rowIndex, selectedUnit.lineIndex);
			var end_pos	  = new Array(rowIndex, lineIndex);
			this.swapPos(start_pos, end_pos);
		}

	});

	return self;
}

