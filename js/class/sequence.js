/**
 * seauence.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Sequence = new function() {
	var core;		//@ Sequence has a enchant.Core Class().

	/**  Model		**/
	var starting;
	var prepare;
	var battle;
	var debug;

	/**  View		**/
	var startingScene;
	var prepareScene;
	var battleScene;
	var debugScene;

	/**  Controller	**/
	var startingController;
	var prepareController;
	var battleController;
	var debugController;

	var self = function Sequence() {
		this.core = new Core(320, 320);
	}

	self.prototype = {
		constructor: self
		/**
		 * Initialize MVC Object in Game
		 */
		, gameInit: function() {
			// Prepare
			this.prepare			= new Prepare();
			this.prepareScene		= new PrepareScene(this.prepare);
			this.prepareController	= new PrepareController(this.prepare, this.prepareScene);
			// Battle
			this.battle				= new Battle(this.core);
			this.battleScene		= new BattleScene(this.battle);
			this.battleController	= new BattleController(this.battle, this.battleScene);
			// Lobby
			this.lobby				= new Lobby();
			this.lobbyScene			= new LobbyScene(this.lobby);
			this.lobbyController	= new LobbyController(this.lobby, this.lobbyScene);
			// Core._scenesに追加
			this.core.pushScene(this.prepareScene);
			this.core.pushScene(this.battleScene);
			this.core.pushScene(this.lobbyScene);
		}

		/**
		 * Game calls StartingScene at first
		 * @see main.js
		 */
		, callStarting: function() {
			this.starting			= new Starting();
			this.startingScene		= new StartingScene(this.debug);
			this.startingController	= new StartingController(this.starting, this.startingScene);
			// Core._scenesに追加
			this.core.pushScene(this.startingScene);
		}

		/**
		 * Scene Manager
		 */
		, removeScene: function(scene) {
			this.core.rootScene.removeChild(scene);
		}
		, changeScene: function(scene_instance) {
			// pushSceneは新しくSceneを追加するので使わない
			//this.core.currentScene = scene_instance;
			//this.core.pushScene(scene_instance);
			this.core.replaceScene(scene_instance);
		}

		/**
		 * Initialize MVC Object in Debug
		 */
		, debugInit: function() {
			this.debug				= new Debug();
			this.debugScene			= new DebugScene(this.debug);
			this.debugController	= new DebugController(this.debug, this.debugScene);
			this.changeScene(this.debugScene);
		}
	};

	return self;
}

