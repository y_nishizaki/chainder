/**
 * arrangement.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Arrangement = new function() {
	var list;
	var units;
	var chainCounter;
	var maxhp, str, def, mgc;

	var self = function Arrangement(list_arg) {
		var default_list = [
			["",  "",  "",  "",  ""],
			["",  "",  "",  "",  ""],
			["",  "",  "",  "",  ""]
		];
		if (typeof list_arg === 'undefined') list_arg = default_list;
		this.setList(list_arg);
		this.update();
	}

	self.prototype = {
		constructor: self
		/**
		 * getter
		 */
		, getMaxHp: function() {
			this.maxhp = 0;
			for (var i=0; i<this.units.length; i++) {
				var unit = this.units[i];
				this.maxhp += unit.getMaxHp();
			}
			return this.maxhp;
		}
		, getStr: function() {
			this.str = 0;
			for (var i=0; i<this.units.length; i++) {
				var unit = this.units[i];
				this.str += unit.getStr();
			}
			return this.str;
		}
		, getDef: function() {
			this.def = 0;
			for (var i=0; i<this.units.length; i++) {
				var unit = this.units[i];
				this.def += unit.getDef();
			}
			return this.def;
		}
		, getMgc: function() {
			this.mgc = 0;
			for (var i=0; i<this.units.length; i++) {
				var unit = this.units[i];
				this.mgc += unit.getMgc();
			}
			return this.mgc;
		}
		, getUnits: function() {
			return this.units;
		}
		, getList: function() {
			return this.list;
		}

		/**
		 * setter
		 */
		, setUnits: function(units) {
			// Formationからunitsを受け取りパラメータを更新する
			this.units = units;
		}
		, setList: function(list_arg) {
			// list_argからlistとunitsを新しく生成する
			this.list = list_arg;
			this.units = new Array();
			for (var i=0; i<this.list.length; i++) {
				for (var j=0; j<this.list[i].length; j++) {
					if (this.list[i][j] == "")	continue;
					var job_name = this.list[i][j];
					var unit = new Unit(job_name);
					unit.rowIndex  = j;
					unit.lineIndex = i;
					this.units.push(unit);
				}
			}
		}

		/**
		 * list manager
		 */
		, searchEmpty: function() {
			for (var i=0; i<this.list.length; i++) {
				for (var j=0; j<this.list[i].length; j++) {
					if (this.list[i][j]	== "" ) {
						var rowIndex  = j;
						var lineIndex = i;
						return new Array(rowIndex, lineIndex);
					}
				}
			}
			return false;
		}
		, searchUnit: function(rowIndex, lineIndex) {
			for (var i=0; i<this.units.length; i++) {
				if (this.units[i].rowIndex == rowIndex && this.units[i].lineIndex == lineIndex) {
					return this.units[i];
				}
			}
			return false;
		}

		/**
		 *  update
		 */
		, update: function() {
			// List update
			var default_list = [
				["",  "",  "",  "",  ""],
				["",  "",  "",  "",  ""],
				["",  "",  "",  "",  ""]
			];
			// listの初期化
			var list = default_list;
			// this.unitsの行と列からlistを生成する
			for (var i=0; i<this.units.length; i++) {
				var unit = this.units[i];
				var row  = unit.rowIndex;
				var line = unit.lineIndex;
				list[line][row] = unit.getJobAbbr();
			}
			this.list = list;

			// Status update
			for (var i=0; i<this.list.length; i++) {
				for (var j=0; j<this.list[i].length; j++) {
					// 空の場合
					if (this.list[i][j] == "")	continue;

					// chain を調べる
					this.chainCounter = new ChainCounter(this.list, i, j);
					var chain_params = {
						'sameJobChain' : this.chainCounter.getSameJobChain(),
						'brChain' : this.chainCounter.getBrChain(),
						'trChain' : this.chainCounter.getTrChain(),
						'lonely'  : this.chainCounter.isLonely()
					}
					var chain = new Chain(chain_params);

					// unit のパラメータを計算
					var unit = this.searchUnit(j, i);
					if (unit) {
						unit.setChain(chain);
						unit.update();
					}
				}
			}
		}

		/**
		 * debug method
		 */
		, viewList: function() {
			var str ="";
			for (var i=0; i<this.list.length; i++) {
				for (var j=0; j<this.list[i].length; j++) {
					var text = this.list[i][j];
					while (text.length < 3) {
						text = " " + text;
					}
					// strにlistの中身を足していく
					str += text + ",";
				}
				str += "\n";
			}
			return str;
		}
		, viewStatus: function() {
			var str ="";
			str += "HP :" + this.getMaxHp() + ", ";
			str += "str:" + this.getStr() + ", ";
			str += "def:" + this.getDef() + ", ";
			str += "mgc:" + this.getMgc() + ", ";
			return str;
		}
	};

	return self;
}


