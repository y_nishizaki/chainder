/**
 * debug.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Debug = new function() {

	var self = function Debug() {
		SceneState.apply(this, arguments);
	}

	var uber = SceneState.prototype;

	self.prototype = extend(uber, {
		constructor: self
	});

	return self;
}

