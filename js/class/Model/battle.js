/**
 * battle.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Battle = new function() {
	var core;	//@ reference to Sequence.core
	var enemy;
	var friends;
	var turn;
	var phase;	// 0:attack, 1:tactics

	var self = function Battle(core) {
		SceneState.apply(this, arguments);
		this.core		= core;			// Seauence.coreの参照渡し
		this.enemy		= new Battler();
		this.friends	= new Battler();
		this.phase = 0;					// 0:attack, 1:tactics
		this.turn  = 0;					// battle turn
		this.result  = 0;				// 0:in battle, 1:win, 2:lose, 3:drawGame
		this._last_f_damage = 0;		// 最後に味方が受けたダメージを記録しておく
		this._last_e_damage = 0;		// 最後に敵に与えたダメージを記録しておく
		this._last_f_damage_type = false;	// 0:Miss, 1:Normal, 2:Critical
		this._last_e_damage_type = false;	// 0:Miss, 1:Normal, 2:Critical
		this._attack_progress  = 0;		// 0:init, 1:waiting. 2:finish
		this._attack_wait_process   = 0;	// 0:atk effect, 1:in atk, 2:damage
		this._attack_duration   = 8;	// AttackEffect 待ちフレーム数
		this._attack_duration2  = 35;	// DamageEffect 待ちフレーム数
		this._attack_waiting    = 0; 
		this._attack_waiting2   = 0; 
		this._tactics_progress = 0;		// 0:init, 1:waiting. 2:finish
		this._in_battle = false;		// バトル中ならばtrue
		this.count		= 0;			// 戦闘回数

		// Debugで戦闘のテスト用にデータを用意する
		if (Game.Debug) {
			// Test
			var test = new Test();
			// Enemy
			test.setUnitList(1);
			var enemy_name = "軍神テュール";
			this.enemy.setBattlerName(enemy_name);
			var e_list = test.getUnitList();
			this.enemy.setList(e_list);
			// Friends
			test.setUnitList(2);
			var f_list = test.getUnitList();
			this.friends.setList(f_list);
		}
	}

	var uber = SceneState.prototype;

	self.prototype = extend(uber, {
		constructor: self
		/**
		 * getter
		 */
		, getPhase: function() {
			return this.phase;
		}
		, getTurn: function() {
			return this.turn;
		}
		, getAttackProgress: function() {
			return this._attack_progress;
		}
		, getTacticsProgress: function() {
			return this._tactics_progress;
		}
		, getAttackWaitProcess: function() {
			return this._attack_wait_process;
		}
		, getCount: function() {
			return this.count;
		}

		/**
		 * boolean
		 */
		, isBeforeBattle: function() {
			if (this._in_battle == false) {
				return true;
			}
			return false;
		}
		, isInBattle: function() {
			return this._in_battle;
		}
		, isAttackWaiting: function() {
			if (this._attack_waiting < this._attack_duration) {
				return true;
			}
			return false;
		}

		/**
		 * setter
		 */
		, setBattleState: function(state) {
			if (state == "start") {
				this._in_battle = true;
			}
			else if (state == "end") {
				this._in_battle = false;
			}
		}
		, setEnemyFormation: function(formation) {
			this.enemy.setFormation(formation);
		}
		, setFriendsFormation: function(formation) {
			this.friends.setFormation(formation);
		}

		/**
		 * reset
		 */
		, reset: function() {
			this.phase = 0;					
			this.turn  = 0;					
			this.result  = 0;				
			this._last_f_damage = 0;
			this._last_e_damage = 0;
			this._attack_progress  = 0;		
			this._attack_waiting   = 0; 
			this._tactics_progress = 0;		
			this._in_battle = false;		
		}

		/**
		 * Battle Count
		 */
		, countUp: function() {
			this.count++;
		}

		/**
		 * Battle Phase Manager
		 */
		, changePhase: function(p_name) {
			if (p_name == "attack") {
				this.phase = 0;
				// Attack Phaseの進捗をリセット
				this.changeAttackProgress("init");
			} 
			else if (p_name == "tactics") {
				this.phase = 1;
				// Tactics Phaseの進捗をリセット
				this.changeTacticsProgress("init");
			}
		}

		/**
		 * AttackPhase Process
		 */
		, attackPhase: function() {
			// Turn開始
			this.turn += 1;
			// FriendsがEnemyに攻撃
			var e_damage_type	= this.calcDamageType(this.friends);
			var e_damage		= this.calcDamage(this.friends, this.enemy, e_damage_type);
			this.enemy.damage(e_damage);
			this._last_e_damage		 = e_damage;
			this._last_e_damage_type = e_damage_type;
			// EnemyがFriendsに攻撃
			var f_damage_type	= this.calcDamageType(this.enemy);
			var f_damage		= this.calcDamage(this.enemy, this.friends, f_damage_type);
			this.friends.damage(f_damage);
			this._last_f_damage = f_damage;
			this._last_f_damage_type = f_damage_type;
			// Judge
			this.judge();
			// Debug Console
			if (Game.Debug) {
				console.log( " - - - - - - - - - - - - - - - - - - - -" );
				console.log("＊turn " + this.turn );
				console.log("[Attack Phase]");
				console.log("- friendsのType：" + this.friends.getTypeAlias() + ". enemyのType：" + this.enemy.getTypeAlias());
				console.log("- enemy  に " + e_damage + "のダメージ " + this.getDamageTypeStr(e_damage_type));
				console.log("- friendsに " + f_damage + "のダメージ " + this.getDamageTypeStr(f_damage_type));
				console.log("- friends_hp", this.friends.hp + "/" + this.friends.maxhp); 
				console.log("- enemy_hp  ", this.enemy.hp + "/" + this.enemy.maxhp); 
			}
		}

		/**
		 * Battle Damage Calculation Formula
		 */
		, calcDamage: function(attacker, defencer, damage_type) {
			var damage = 0;
			if (Game.DAMAGE_TYPE == 1) {
				// 攻撃バトラーのタイプ
				var atk_type = attacker.getType();
				// 基礎ダメージ値を算出
				switch (atk_type) {
					case "str" :
						damage = attacker.getStr() - defencer.getDef();
						break;
					case "def" :
						damage = attacker.getDef() - defencer.getMgc();
						break;
					case "mgc" :
						damage = attacker.getMgc() - defencer.getStr();
						break;
					default:
						break;
				}
				if (Game.Debug) {
					// console.log("基礎ダメージ:" + damage);
				}

				// 攻撃バトラーのタイプがstrの場合の補正
				if (atk_type == "str") {
					damage += 10;
				}
				// 防御バトラーのタイプがdefの場合の補正
				if (defencer.getType() == "def") {
					damage -= 10;
				}

				// 攻撃タイプに従いダメージ補正をかける
				switch (damage_type) {
					case 0 :	// Miss
						damage = Math.floor(damage * 0.5);
						break;
					case 1 :	// Normal
						// 通常ダメージはx0.9からx1.2の間で変動する
						var rand	= Math.floor(Math.random()*30);
						var bonus	= (rand + 90) / 100;		// 0.9 - 1.2
						damage = Math.floor(damage * bonus);
						if (Game.Debug) {
							// console.log("通常ダメージ倍率:" + bonus);
						}
						break;
					case 2 :	// Critical
						damage = Math.floor(damage * 1.5);
						break;
					default:
						break;
				}

				// Damageを調整
				if (damage <= 0)	damage = 0;

			} else {
				// 物理攻撃は守備で受ける
				var str_damage	= attacker.getStr() - defencer.getDef();
				// 魔法攻撃は魔法でうける
				var mgc_damage	= attacker.getMgc() - defencer.getMgc();
				// Damageを調整
				if (str_damage <= 0)	str_damage = 0;
				if (mgc_damage <= 0)	mgc_damage = 0;
				damage = str_damage + mgc_damage;
			}
			// Damageは整数(切り捨て)
			damage = parseInt(damage);
			return damage;
		}

		/**
		 * Battle Damage Tyep Calculation Formula
		 */
		, calcDamageType: function(attacker) {
			// Missやクリティカルの確率を算出する
			var damage_type = 0;	// 0:Miss, 1:Normal, 2:Critical
			// 攻撃バトラーのクリティカル率を取得
			var crt		= attacker.getCrt();
			var rand	= Math.floor(Math.random()*100);
			// ダメージタイプの判別
			if (rand <= crt) {					// クリティカル！
				damage_type = 2;
			}
			else if (rand  <=  (95 - crt) ) {	// 通常攻撃
				damage_type = 1;
			}
			else {								// Miss
				damage_type = 0;
			}
			return damage_type;
		}
		, getDamageTypeStr: function(damage_type) {
			var str = "";
			switch (damage_type) {
				case 0:
					str = "Miss...";
					break;
				case 1:
					str = "Normal";
					break;
				case 2:
					str = "Critical!";
					break;
				default:
					break;
			}
			return str;
		}

		, tacticsPhase: function() {
			// Random Shuffle
			this.friends.shuffleFormation();
			this.enemy.shuffleFormation();
			// Debug Console
			if (Game.Debug) {
				console.log("[Tactics Phase]");
				console.log("friends status");
				this.friends.formation.arrangement.viewStatus();
				//this.friends.formation.arrangement.viewList();
				console.log("enemy status");
				this.enemy.formation.arrangement.viewStatus();
				//this.enemy.formation.arrangement.viewList();
			}
		}

		/**
		 * in Battle Phase, Progress Manager
		 */
		, changeAttackProgress: function(p_name) {
			if (p_name == "init") {
				this._attack_progress = 0;
			} 
			else if (p_name == "waiting") {
				this._attack_progress = 1;
				// 待ち時間のカウントをリセット
				this._attack_waiting      = 0;
				this._attack_waiting2     = 0;
				this._attack_wait_process = 0;
			}
			else if (p_name == "finish") {
				this._attack_progress = 2;
			}
		}
		, changeTacticsProgress: function(p_name) {
			if (p_name == "init") {
				this._tactics_progress = 0;
			} 
			else if (p_name == "waiting") {
				this._tactics_progress = 1;
			}
			else if (p_name == "finish") {
				this._tactics_progress = 2;
			}
		}

		/**
		 * 攻撃とダメージエフェクトの待ち時間処理
		 */
		, countAttackWait: function() {
			var atk_wait_process = this.getAttackWaitProcess();
			switch (atk_wait_process) {
				case 0:
					this._attack_wait_process = 1;
					break;
				case 1:
					if (this._attack_waiting < this._attack_duration) {
						this._attack_waiting += 1;
					} else {
						this._attack_wait_process = 2;
					}
					break;
				case 2:
					this._attack_wait_process = 3;
					break;
				case 3:
					if (this._attack_waiting2 < this._attack_duration2) {
						this._attack_waiting2 += 1;
					} else {
						this._attack_wait_process = 4;
					}
					break;
				case 4:
					this._attack_wait_process = 5;
					break;
				case 5:
					break;
				default:
					break;
			}
		}

		/**
		 * Battle Result Manager
		 */
		, judge: function() {
			if (this.friends.isLive() && this.enemy.isDead()) {
				this.result = 1; // win
			}
			else if (this.friends.isDead() && this.enemy.isLive()) {
				this.result = 2; // lose
			} 
			else if (this.friends.isDead() && this.enemy.isDead()) {
				this.result = 3; // draw game
			}
		}
		, getResult: function() {
			if (this.result == 0) {
				return false;
			} else if (this.result == 1) {
				return "WIN";
			} else if (this.result == 2) {
				return "LOSE";
			} else if (this.result == 3) {
				return "DRAW";
			}
			return false;
		}
		, win: function() {
			// console.log("プレイヤーの勝利です");
			// Finish Battle
			this.setBattleState("end");
		}
		, lose: function() {
			// console.log("Enemyの勝利です");
			// Finish Battle
			this.setBattleState("end");
		}
		, drawGame: function() {
			// console.log("引き分け");
			// Finish Battle
			this.setBattleState("end");
		}

	});

	return self;
}


