/**
 * starting.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var Starting = new function() {
	var cleard;

	var self = function Starting() {
		SceneState.apply(this, arguments);
		this.cleard = false;
	}

	var uber = SceneState.prototype;

	self.prototype = extend(uber, {
		constructor: self
	});

	return self;
}

