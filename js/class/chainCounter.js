/**
 * chainCounter.js
 *
 * @author	 Yuya Nishizaki <nishizaki.yuya@gmail.com>
 * @license	 http://www.opensource.org/licenses/mit-license.php The MIT License
 */

var ChainCounter = new function() {
	var list;
	var sameJobChain, brChain, trChain;
	var rowIndex, lineIndex;

	var self = function ChainCounter(list_arg, lineIndex, rowIndex) {
		if (typeof list_arg === 'undefined')	return false;
			this.list = list_arg;

		if (typeof lineIndex === 'undefined')	return false;
			this.lineIndex = lineIndex;

		if (typeof rowIndex === 'undefined')	return false;
			this.rowIndex = rowIndex;
	}

	self.prototype = {
		constructor: self
		/**
		 *	getter method
		 */
		, getSameJobChain: function() {
			this.countSameJobChain();
			return this.sameJobChain;
		}
		, getBrChain: function() {
			this.countBrChain();
			return this.brChain;
		}
		, getTrChain: function() {
			this.countTrChain();
			return this.trChain;
		}

		/**
		 *	isLonely method
		 */
		, isLonely: function() {
			for (var i=this.lineIndex-1; i<=this.lineIndex+1; i++) {
				for (var j=this.rowIndex-1; j<=this.rowIndex+1; j++) {
					if (this.lineIndex==i && this.rowIndex==j)	continue;
					if (i < 0 || j < 0) continue;
					if (2 < i || 4 < j) continue;
					if (typeof this.list[i][j] === 'undefined') continue;
					if (this.list[i][j])	return false;
				}
			}
			return true;
		}

		/**
		 *  array's manipulator method
		 */
		, getLine: function(lineIndex) {
			if (typeof lineIndex === 'undefined') lineIndex = this.lineIndex;
			return this.list[lineIndex];
		}
		, getRow: function(rowIndex) {
			if (typeof rowIndex === 'undefined') rowIndex = this.rowIndex;
			var row_array = new Array();
			for (var i=0; i<this.list.length; i++) {
				row_array.push(this.list[i][rowIndex]);
			}
			return row_array;
		}

		/**
		 *  SameJobChain counter method
		 */
		, countSameJobChain: function() {
			//タテ
			var row = this.getRow();
			var line_count = this.searchSameJobChain(row, this.lineIndex);
			//ヨコ
			var line = this.getLine();
			var row_count = this. searchSameJobChain(line, this.rowIndex);
			//合計
			this.sameJobChain  = new Object();
			this.sameJobChain.row  = row_count;
			this.sameJobChain.line = line_count;
			return this.sameJobChain;
		}
		, searchSameJobChain: function(arr, index) {
			if (arr[index] == "") return false;

			var count = 0;
			if (arr[index-1] !== 'undefined') {
				if (arr[index-1] == arr[index]) {
					count++;
				}
			}
			if (arr[index+1] !== 'undefined') {
				if (arr[index+1] == arr[index]) {
					count++;
				}
			}
			return count;
		}

		/**
		 *  BrChain counter method
		 */
		, countBrChain: function() {
			//タテ
			var row = this.getRow();
			var line_count = this.searchBrChain(row, this.lineIndex);
			//ヨコ
			var line = this.getLine();
			var row_count = this.searchBrChain(line, this.rowIndex);
			//合計
			this.brChain  = new Object();
			this.brChain.row  = row_count;
			this.brChain.line = line_count;
			return this.brChain;
		}
		, searchBrChain: function(arr, index) {
			if (arr[index] == "") return false;

			var count = 0;
			var arr_before	= arr.slice(0, index+1).reverse();
			var arr_after	= arr.slice(index, arr.length);
			count = this.getSandwichedBr(arr_before) + this.getSandwichedBr(arr_after);
			return count;
		}
		, getSandwichedBr: function(arr) {
			if (arr[0]=="Br" || arr[0]=="Tr") return 0; //Br自身とTrはボーナスの適用外

			var count = 0; 
			var stack = 0;
			for (var i=0; i<arr.length; i++) {
				if (i==0) continue;	//自分自身はスルー
				if (arr[i] == arr[0]) {
					// console.log("BrCountを" + stack + "個追加しました");
					count += stack; //同じJobで囲まれたBr
					stack = 0;
					continue; 
				}
				if (arr[i] == "Br") {
					stack++;	//Brを検出
					continue;
				}
				return count;	//同じJobまたはBr以外の場合終了
			}
			return count;
		}

		/**
		 *  TrChain counter method
		 */
		, countTrChain: function() {
			//タテ
			var row = this.getRow();
			var line_count = this.searchTrChain(row, this.lineIndex);
			//ヨコ
			var line = this.getLine();
			var row_count = this.searchTrChain(line, this.rowIndex);
			//合計
			this.trChain  = new Object();
			this.trChain.row  = row_count;
			this.trChain.line = line_count;
			return this.trChain;
		}
		, searchTrChain: function(arr, index) {
			if (arr[index] == "") return false;
			var count = 0;
			var arr_before	= arr.slice(0, index+1).reverse();
			var arr_after	= arr.slice(index, arr.length);

			// タテかヨコのどちらを検索するか決める
			var type = "";
			if (arr.length == 5) {
				type = "row";
			} else if (arr.length == 3) {
				type = "line";
			} else {
				return false;
			}
			// another chain
			var sameJobChain = this.countSameJobChain();
			var brChain		 = this.countBrChain();
			// tr chain 
			if (type == "row") {
				if (sameJobChain.row || brChain.row) {
					count = this.getConnectTr(arr_before) + this.getConnectTr(arr_after);
				} else {
					count = 0;
				}
			}
			else if (type == "line") {
				if (sameJobChain.line || brChain.line) {
					count = this.getConnectTr(arr_before) + this.getConnectTr(arr_after);
				} else {
					count = 0;
				}
			}
			return count;
		}
		, getConnectTr: function(arr) {
			if (arr[0]=="Br" || arr[0]=="Tr") return 0; //Br自身とTrはボーナスの適用外

			var count = 0;
			var br_stack = 0;
			var tr_index = arr.indexOf("Tr");
			if (tr_index == -1) return 0; //Trが見つからない場合

			var arr		 = arr.slice(0, tr_index+1);
			for (var i=0; i<arr.length; i++) {
				if (i==0) continue;	//自分自身はスルー
				else if (arr[i] == arr[0]) {
					br_stack = 0;
					continue;  //同じJob
				}
				else if (arr[i] == "Br") {
					br_stack++;
					continue;
				}
				else if (arr[i] == "Tr") {
					if (br_stack) return 0;  // Bridgeで途切れている場合
					else count++;
				} 
				else {
					return 0;
				}
			}
			return count;
		}

	};

	return self;
}


