/**
 * Chaineder MVC Framework
 *   Game has a Sequence Object.
 *   Sequence has 
 * 		an enchant.js core object,	
 *	and	Application Models,			(Model)
 * 	and enchant.js Scene Clases.	(View) 
 * 	and	Application Controllers.	(Controller)
 *
 *   Controller is assosiated EventTarget object.
 *   ex)	command1.addEventListener(enchant.Event.TOUCH_START, function(e) {
 * 			Game.Seq.StartingController.hoge();
 * 		}
 */

enchant();

var Game;				// Global Object
Game.Debug			= false;		// Debug Settings
Game.DAMAGE_TYPE	= 1;		// Damage Settings
Game.StockDebug		= false;		// Stock x 99

window.onload = function() {
	/**
	 *  ---------------------------------
	 *   enchant.js Application Settings
	 *  ---------------------------------
	 */
	var Seq = new Sequence();
	Seq.core.fps = 30;
	Seq.core.preload('images/loa/element.png');
	Seq.core.preload('images/loa/bg01.png');
	Seq.core.preload('images/loa/bg02.png');
	Seq.core.preload('images/loa/bg03.png');
	Seq.core.preload('images/loa/battle_status.png');
	Seq.core.preload('images/loa/battle.png');
	Seq.core.preload('images/loa/prepare_status.png');
	Seq.core.preload('images/loa/prepare_stock.png');
	Seq.core.preload('images/loa/prepare_enemy.png');
	Seq.core.preload('images/loa/formation_frame.png');
	Seq.core.preload('images/loa/lobby.png');
	Seq.core.preload('images/loa/title.png');
	Seq.core.preload('images/loa/touch_start_mini.png');
	Seq.core.preload('images/loa/battle_start_01_mini.png');
	Seq.core.preload('images/loa/battle_start_02_mini.png');
	Seq.core.preload('images/loa/title.png');
	Seq.core.preload('images/loa/opening.png');
	Seq.core.preload('images/loa/ending.png');
	Seq.core.preload('images/loa/fire.png');
	Seq.core.preload('images/loa/blizzard.png');
	Seq.core.preload('images/loa/thunder.png');
	Seq.core.preload('images/loa/manual01.png');
	Seq.core.preload('images/loa/manual02.png');
	Seq.core.preload('images/loa/manual03.png');
	Seq.core.preload('images/loa/manual04.png');
	Seq.core.preload('images/loa/number01.png');
	Seq.core.preload('images/loa/number02.png');
	Seq.core.preload('images/loa/line01.png');
	Seq.core.preload('images/loa/line02.png');
	Seq.core.preload('images/loa/line03.png');
	Seq.core.preload('images/loa/hp_bar.png');
	Seq.core.preload('images/loa/hp_frame.png');
	Seq.core.preload('images/loa/crystal.png');
	Seq.core.preload('images/loa/btn_next.png');
	Seq.core.preload('images/loa/btn_manual.png');
	Seq.core.preload('images/loa/btn_back.png');
	Seq.core.preload('images/loa/vs.png');
	Seq.core.preload('images/loa/congratulations.png');

	/**
	 *  --------------------------------
	 *      Chaineder Game Settings
	 *  --------------------------------
	 */
	Seq.core.onload = function() {

		/** 
		 * Chaineder Test Case
		 */
		var test = new Test();
		//  test.sequenceTest(Seq);
		//  test.setUnitList("0002");
		//  test.arrangementTest();
		//  test.chainCounterTest();
		//  test.unitManagerTest();
		//  test.stockTest();
		//  test.formationTest();
		//  test.prepareTest();
		//  test.battleTest();
		//  test.challengerTest();
		//  test.lobbyTest();
		//  test.synchroTest();

		/** 
		 * Starting Scene
		 */
		Seq.callStarting();
		if (!Game.Debug) {
			Seq.startingController.NewGame();
		}
	}

	/**
	 *  enchant.js Game Start
	 */
	Seq.core.start();
	Game.Seq = Seq;
};



